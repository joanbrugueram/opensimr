/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Parser.h"
#include "Utils/StrFmt.h"
#include <stdexcept>

 /// <summary>Thrown when an error happens while parsing a MR program.</summary>
class ParserException : public std::runtime_error
{
public:
	/// <summary>Create a new MR parser exception.</summary>
	/// <param name="nLine">The line where the error happened.</param>
	/// <param name="message">The error message.</param>
	ParserException(size_t nLine, const std::string &message)
		: std::runtime_error(StrFmt() << "MR Parser - Line " << nLine << ": " << message)
	{
	}
};

OpenSiMR::Parser::Parser(const StringChunk &code)
	: lex(code) { }

bool OpenSiMR::Parser::isEnd() const
{
	return lex.isEndOfString();
}

OpenSiMR::SourceLine OpenSiMR::Parser::parseLine()
{
	SourceLine parsedLine(lex.getLine());
	size_t nLine = lex.getLineNumber();

	while (!lex.isEndOfLine())
	{
		StringChunk mainToken = lex.readWord();
		if (mainToken.empty())
			throw ParserException(nLine, "Expecting word at start of expression.");

		if (mainToken == ".begin")
		{
			StringChunk label;

			if ((label = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting label following .begin directive.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after .begin directive.");

			parsedLine.statements.push_back(Statement(mainToken).add(label));
		}
		else if (mainToken == ".dw")
		{
			Statement words = Statement(mainToken);

			while (true)
			{
				StringChunk word;
				if ((word = lex.readWord()).empty())
					throw ParserException(nLine, "Expecting word following .dw directive.");
				words.add(word);

				if (lex.isEndOfLine())
					break;
				if (lex.readSeparator() != ",")
					throw ParserException(nLine, "Expecting ',' following .dw value.");
			}

			parsedLine.statements.push_back(words);
		}
		else if (mainToken == ".rw")
		{
			StringChunk nWords;
			if ((nWords = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting word following .rw directive.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after .rw directive.");

			parsedLine.statements.push_back(Statement(mainToken).add(nWords));
		}
		else if (mainToken == ".end")
		{
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after .end directive.");

			parsedLine.statements.push_back(Statement(mainToken));
		}
		else if (mainToken == "load")
		{
			StringChunk addrBase, addrReg, dstReg;
			if ((addrBase = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting base address as load's first operand.");
			if (lex.readSeparator() != "(")
				throw ParserException(nLine, "Expecting '(' after load's base address.");
			if ((addrReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting offset inside load's parenthesis.");
			if (lex.readSeparator() != ")")
				throw ParserException(nLine, "Expecting ')' after load's offset.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after load's first operand.");
			if ((dstReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting destination as load's second operand.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after load's two operands.");

			parsedLine.statements.push_back(Statement(mainToken).add(addrBase).add(addrReg).add(dstReg));
		}
		else if (mainToken == "store")
		{
			StringChunk srcReg, addrBase, addrReg;
			if ((srcReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting destination as store's first operand.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after store's destination.");
			if ((addrBase = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting base address as store's second operand.");
			if (lex.readSeparator() != "(")
				throw ParserException(nLine, "Expecting '(' after store's base address.");
			if ((addrReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting offset inside store's parenthesis.");
			if (lex.readSeparator() != ")")
				throw ParserException(nLine, "Expecting ')' after store's offset.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after store's two operands.");

			parsedLine.statements.push_back(Statement(mainToken).add(srcReg).add(addrBase).add(addrReg));
		}
		else if (mainToken == "br" ||
				 mainToken == "beq" ||
				 mainToken == "bl" ||
				 mainToken == "ble" ||
				 mainToken == "bne" ||
				 mainToken == "bge" ||
				 mainToken == "bg")
		{
			StringChunk dst;
			if ((dst = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting address following jump.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after jump's one operand.");

			parsedLine.statements.push_back(Statement(mainToken).add(dst));
		}
		else if (mainToken == "add" || mainToken == "sub" || mainToken == "and")
		{
			StringChunk src1Reg, src2Reg, dstReg;
			if ((src1Reg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting source as alu-reg's first operand.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after alu-reg's first operand.");
			if ((src2Reg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting source as alu-reg's second operand.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after alu-reg's second operand.");
			if ((dstReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting destination as alu-reg's third operand.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after alu-reg's three operands.");

			parsedLine.statements.push_back(Statement(mainToken).add(src1Reg).add(src2Reg).add(dstReg));
		}
		else if (mainToken == "asr")
		{
			StringChunk srcReg, dstReg;
			if ((srcReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting source as asr's first operand.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after asr's first operand.");
			if ((dstReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting destination as asr's second operand.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after asr's two operands.");

			parsedLine.statements.push_back(Statement(mainToken).add(srcReg).add(dstReg));
		}
		else if (mainToken == "addi" || mainToken == "subi")
		{
			StringChunk srcReg, immediate, dstReg;
			if ((srcReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting source as alu-imm's first operand.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after alu-imm's first operand.");
			if (lex.readSeparator() != "#")
				throw ParserException(nLine, "Expecting '#' before alu-imm's immediate.");
			if ((immediate = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting immediate as alu-imm's second operand.");
			if (lex.readSeparator() != ",")
				throw ParserException(nLine, "Expecting comma after alu-imm's second operand.");
			if ((dstReg = lex.readWord()).empty())
				throw ParserException(nLine, "Expecting destination as alu-imm's third operand.");
			if (!lex.isEndOfLine())
				throw ParserException(nLine, "Expecting end of line after alu-imm's three operands.");

			parsedLine.statements.push_back(Statement(mainToken).add(srcReg).add(immediate).add(dstReg));
		}
		else
		{
			StringChunk sep = lex.readSeparator();
			if (sep == ":")
			{
				if (parsedLine.labels.size() > 0)
					throw ParserException(nLine, "Only one label is accepted per line.");

				parsedLine.labels.push_back(Label(mainToken));
			}
			else if (sep == "=")
			{
				StringChunk value;

				if ((value = lex.readWord()).empty())
					throw ParserException(nLine, "Expecting value following constant declaration.");
				if (!lex.isEndOfLine())
					throw ParserException(nLine, "Expecting end of line after constant value.");

				parsedLine.constants.push_back(Constant(mainToken, value));
			}
			else
				throw ParserException(nLine, StrFmt() << "Expecting ':' or '=' after identifier" <<
					" (misspelled instuction '" << mainToken << "'?).");
		}
	}
	lex.readEndOfLine();

	return parsedLine;
}