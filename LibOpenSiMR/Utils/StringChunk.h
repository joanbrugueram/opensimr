/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_UTILS_STRINGCHUNK_H
#define OPENSIMR_UTILS_STRINGCHUNK_H

#include <ostream>
#include <string>
#include <cstring>

/// <summary>A wrapper around a chunk of a string. Performs case-insensitive comparisons.</summary>
/// <remarks>
/// A StringChunk does never hold the memory for the string.
/// The user is responsible for keeping the original string alive.
/// </remarks>
class StringChunk
{
	/// <summary>Pointer to the first character of the string.</summary>
	const char *ptr;
	/// <summary>Length of the string.</summary>
	size_t len;

	/// <summary>Does a case-insensitive comparison of two StringChunk.</summary>
	/// <returns>
	/// A negative value if first<second.
	/// Zero if first==second.
	/// A positive value if first>second.
	/// </returns>
	static int cmp(const StringChunk &first, const StringChunk &second)
	{
		for (size_t pos = 0; pos < first.length() && pos < second.length(); pos++)
		{
			if (tolower(first[pos]) != tolower(second[pos]))
				return (int)tolower(first[pos]) - (int)tolower(second[pos]);
		}
		if (first.length() < second.length())
			return -1;
		else if (first.length() > second.length())
			return 1;
		return 0;
	}

public:
	/// <summary>Create an empty StringChunk.</summary>
	StringChunk()
		: ptr(NULL), len(0) { }

	/// <summary>Create a StringChunk from a std::string.</summary>
	/// <param name="str">The C++ standard std::string.</param>
	StringChunk(const std::string &str)
		: ptr(str.c_str()), len(str.length()) { }

	/// <summary>Create a StringChunk from a C-style string.</summary>
	/// <param name="ptr">The C-style (NUL-terminated) string.</param>
	StringChunk(const char *ptr)
		: ptr(ptr), len(std::strlen(ptr)) { }

	/// <summary>Create a StringChunk from an array of characters.</summary>
	/// <param name="ptr">A pointer to the first character.</param>
	/// <param name="len">The length of the array, in bytes.</param>
	StringChunk(const char *ptr, size_t len)
		: ptr(ptr), len(len) { }

	/// <summary>Get the character at the specified index of the string.</summary>
	/// <param name="idx">The index in the string.</param>
	/// <returns>The character at the specified index.</returns>
	const char &operator[](const size_t idx) const
	{
		return ptr[idx];
	}

	/// <summary>Get the length of the string, in bytes.</summary>
	/// <returns>The length of the string, in bytes.</summary>
	size_t length() const
	{
		return len;
	}

	/// <summary>Checks if this StringChunk is an empty string.</summary>
	/// <returns>true if it's an empty string, false otherwise.</returns>
	bool empty() const
	{
		return len == 0;
	}

	/// <summary>Create a substring starting at the specified index and ending at the end of this StringChunk.</summary>
	/// <param name="subIdx">The index at which the substring starts.</param>
	/// <returns>A substring with the specified parameters.</returns>
	StringChunk substr(size_t subIdx) const
	{
		return substr(subIdx, length() - subIdx);
	}

	/// <summary>Create a substring starting at the specified index and with the specified length.</summray>
	/// <param name="subIdx">The index at which the substring starts.</param>
	/// <param name="subLen">The length of the substring.</param>
	/// <returns>A substring with the specified parameters.</returns>
	StringChunk substr(size_t subIdx, size_t subLen) const
	{
		return StringChunk(ptr + subIdx, subLen);
	}

	/// <summary>Create a std::string from this StringChunk.</summary>
	/// <returns>A C++ standard std::string.</returns>
	std::string toStdString() const
	{
		return std::string(ptr, ptr+len);
	}

	/* Comparison operators. */
	bool operator ==(const StringChunk &other) const { return cmp(*this, other) == 0; }
	bool operator !=(const StringChunk &other) const { return cmp(*this, other) != 0; }
	bool operator <(const StringChunk &other) const  { return cmp(*this, other) < 0; }
};

/// <summary>std::ostream insertion operator for StringChunk.</summary>
/// <param name="os">The std::ostream where the StringChunk should be inserted.</param>
/// <param name="str">The StringChunk to insert.</param>
/// <returns>The same std::ostream that has been passed as a parameter.</returns>
static std::ostream &operator <<(std::ostream &os, const StringChunk &str)
{
	os.write(&str[0], str.length());
	return os;
}

#endif // OPENSIMR_UTILS_STRINGCHUNK_H