/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_UTILS_STRFMT_H
#define OPENSIMR_UTILS_STRFMT_H

#include <sstream>

/// <summary>Utility class designed for quick inline formatting.</summary>
/// <example>std::string something = StrFmt() << "hello" << 123;</example>
class StrFmt
{
	std::ostringstream ss;
public:
	/// <summary>Insertion operator, with the C++ std::iostream syntax.</summary>
	/// <param name="val">The value to insert.</param>
	template<typename T>
	StrFmt &operator <<(T const &val)
	{
		ss << val;
		return *this;
	}

	/// <summary>Get the resulting formatted string.</summary>
	operator std::string()
	{
		return ss.str();
	}
};

#endif // OPENSIMR_UTILS_STRFMT_H
