/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Lexer.h"
#include <cctype>

bool OpenSiMR::Lexer::isCommentChar(char c)
{
	return c == ';';
}

bool OpenSiMR::Lexer::isSeparatorChar(char c)
{
	return c == ':' || c == '=' || c == '#' || c == ',' || c == '(' || c == ')';
}

bool OpenSiMR::Lexer::isWordChar(char c)
{
	return isalnum(c) || c == '.' || c == '_' || c == '+' || c == '-';
}

OpenSiMR::Lexer::Lexer(const StringChunk &str)
	: str(str), pos(0), lineStart(0), nLine(1)
{
	nextToken();
}

StringChunk OpenSiMR::Lexer::getLine() const
{
	size_t end = lineStart;
	while (end < str.length() && str[end] != '\n')
		end++;
	return str.substr(lineStart, end - lineStart);
}

size_t OpenSiMR::Lexer::getLineNumber() const
{
	return nLine;
}

bool OpenSiMR::Lexer::isEndOfString() const
{
	return pos == str.length();
}

bool OpenSiMR::Lexer::isEndOfLine() const
{
	return tokenKind == TokenKind_EndOfLine;
}

bool OpenSiMR::Lexer::readEndOfLine()
{
	if (tokenKind == TokenKind_EndOfLine)
	{
		lineStart = pos;
		nLine++;
		consume();
		return true;
	}

	return false;
}

StringChunk OpenSiMR::Lexer::readSeparator()
{
	return (tokenKind == TokenKind_Separator) ? consume() : StringChunk();
}

StringChunk OpenSiMR::Lexer::readWord()
{
	return (tokenKind == TokenKind_Word) ? consume() : StringChunk();
}

void OpenSiMR::Lexer::nextToken()
{
	// Skip whitespace
	while (pos < str.length() && (isspace(str[pos]) && str[pos] != '\n'))
		pos++;

	// Skip comments
	if (pos < str.length() && isCommentChar(str[pos]))
	{
		while (pos < str.length() && str[pos] != '\n')
			pos++;
	}

	// Figure out token kind
	if (pos == str.length())
	{
		tokenKind = TokenKind_EndOfLine;
		token = StringChunk();
	}
	else if (str[pos] == '\n')
	{
		tokenKind = TokenKind_EndOfLine;
		token = str.substr(pos++, 1);
	}
	else if (isSeparatorChar(str[pos]))
	{
		tokenKind = TokenKind_Separator;
		token = str.substr(pos++, 1);
	}
	else if (isWordChar(str[pos]))
	{
		size_t start = pos;
		while (pos < str.length() && isWordChar(str[pos]))
			pos++;
		tokenKind = TokenKind_Word;
		token = str.substr(start, pos - start);
	}
	else
	{
		tokenKind = TokenKind_Error;
		token = StringChunk();
	}
}

StringChunk OpenSiMR::Lexer::consume()
{
	StringChunk tok = token;
	nextToken();
	return tok;
}