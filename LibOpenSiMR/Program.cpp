/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Program.h"
#include "Assembler.h"
#include <stdexcept>
#include <algorithm>

/// <summary>Thrown when an error happens while loading/saving a program to a stream.</summary>
class ProgramBinaryFormatException : public std::runtime_error
{
public:
	ProgramBinaryFormatException(std::string s)
		: std::runtime_error("SiMR Program Binary Format: " + s)
	{
	}
};

/// <summary>Checks if the endianness of the machine is big endian.</summary>
/// <returns>true if the system is big endian, false otherwise.</returns>
static inline bool isBigEndian()
{
	union EndianTest
	{
		uint8_t bytes[4];
		uint32_t integer;
	} et = { { 0x01, 0x02, 0x03, 0x04 } };

	return et.integer == 0x01020304;
}

/// <summary>Reverses the byte order of the specified value.</summary>
/// <param name="value">A pointer to the value to modify.</summary>
template<typename T>
static void endianSwap(T *value)
{
	if (sizeof(T) == 1)
	{
	}
	else if (sizeof(T) == 2)
	{
		uint16_t *v = (uint16_t *)value;
		*v =
			(*v >> 8) |
			(*v << 8);
	}
	else if (sizeof(T) == 4)
	{
		uint32_t *v = (uint32_t *)value;
		*v =
			((*v >> 24) & 0x000000FF) |
			((*v >> 8)  & 0x0000FF00) |
			((*v << 8)  & 0x00FF0000) |
			((*v << 24) & 0xFF000000);
	}
	else
	{
		throw;
	}
}

/// <summary>Reads a little endian integral value of the specified type from the stream.</summary>
/// <param name="s">The stream from which to read the value.</param>
template<typename T>
static T readLE(std::istream &s)
{
	T value;
	if (!s.read((char *)&value, sizeof(value)))
		throw ProgramBinaryFormatException("Stream read error.");
	if (isBigEndian())
		endianSwap(&value);
	return value;
}

/// <summary>Reads a NUL-terminated string from the specified stream.</summary>
/// <param name="s">The stream from which to read the string.</param>
static std::string readString(std::istream &s)
{
	std::string str;
	if (!std::getline(s, str, '\0'))
		throw ProgramBinaryFormatException("Stream read error.");
	return str;
}

/// <summary>Writes a little endian integral value of the specified type to the stream.</summary>
/// <param name="s">The stream to which the value should be written.</param>
/// <param name="value">The integral value to write.</param>
template<typename T>
static void writeLE(std::ostream &s, T value)
{
	if (isBigEndian())
		endianSwap(&value);
	if (!s.write((char *)&value, sizeof(value)))
		throw ProgramBinaryFormatException("Stream write error.");
}

/// <summary>Writes a NUL-terminated string to the specified stream.</summary>
/// <param name="s">The stream to which the string should be written.</param>
/// <param name="str">The string to write.</param>
static void writeString(std::ostream &s, const std::string &str)
{
	if (!s.write(str.c_str(), str.length()+1))
		throw ProgramBinaryFormatException("Stream write error.");
}

OpenSiMR::Program::Program(const std::string &code)
{
	StringChunk codeStr(code);
	Assembler assembler(code);
	assembler.readConstants();
	assembler.computeAddresses();
	assembler.generateCode(*this);
	assembler.generateMetadata(*this);
}

void OpenSiMR::Program::readFrom(std::istream &s)
{
	// Header
	s.seekg(0);
	initialPc = readLE<uint8_t>(s);
	if (readLE<uint8_t>(s) != 0x00)
		throw ProgramBinaryFormatException("initialPc.high is not zero.");
	for (int i = 0; i < 0x100; i++)
		initialMemory[i] = readLE<int16_t>(s);

	uint32_t lineAddressOffset = readLE<uint32_t>(s);
	uint16_t lineAddressCount = readLE<uint16_t>(s);

	uint32_t symbolTableOffset = readLE<uint32_t>(s);
	uint16_t symbolTableCount = readLE<uint16_t>(s);

	uint32_t sourceLinesOffset = readLE<uint32_t>(s);
	uint16_t sourceLinesCount = readLE<uint16_t>(s);

	// Line <-> Address matchings
	s.seekg(lineAddressOffset);
	lineAddress.clear();

	for (uint16_t i = 0; i < lineAddressCount; i++)
	{
		uint8_t address = readLE<uint8_t>(s);
		if (readLE<uint8_t>(s) != 0)
			throw ProgramBinaryFormatException("lineAddress.address.high is not zero.");
		uint16_t isCode = readLE<uint16_t>(s);
		if (isCode != 0 && isCode != 1)
			throw new ProgramBinaryFormatException("lineAddress.isCode is not bool.");
		uint16_t line = readLE<uint16_t>(s);
		lineAddress.push_back(ProgramLineInfo(address, isCode == 1, line));
	}

	// Symbol table
	s.seekg(symbolTableOffset);
	symbolTable.clear();

	for (uint16_t i = 0; i < symbolTableCount; i++)
	{
		std::string name = readString(s);
		uint16_t type = readLE<uint16_t>(s);
		if (type != ProgramSymbolType_Label && type != ProgramSymbolType_Constant)
			throw ProgramBinaryFormatException("symbolTable.type is not known.");
		uint16_t line = readLE<uint16_t>(s);
		int16_t value = readLE<int16_t>(s);
		symbolTable.push_back(ProgramSymbol(name, (ProgramSymbolType)type, line, value));
	}

	std::reverse(symbolTable.begin(), symbolTable.end()); // See remarks on symbolTable

	// Source Lines
	s.seekg(sourceLinesOffset);
	sourceLines.clear();

	for (uint16_t i = 0; i < sourceLinesCount; i++)
		sourceLines.push_back(readString(s));
}

void OpenSiMR::Program::writeTo(std::ostream &s)
{
	// Check
	if (lineAddress.size() >= 0x10000)
		throw ProgramBinaryFormatException("Too many lineAddress items.");
	if (symbolTable.size() >= 0x10000)
		throw ProgramBinaryFormatException("Too many symbolTable items.");
	if (sourceLines.size() >= 0x10000)
		throw ProgramBinaryFormatException("Too many sourceLines items.");
	for (size_t i = 0; i < lineAddress.size(); i++)
	{
		if (lineAddress[i].line <= 0 || lineAddress[i].line >= 0x10000)
			throw new ProgramBinaryFormatException("lineAddress.line is out of range.");
	}

	// Header
	s.seekp(0);
	writeLE<uint16_t>(s, initialPc);
	for (size_t i = 0; i < 0x100; i++)
		writeLE<int16_t>(s, initialMemory[i]);

	writeLE<uint32_t>(s, 0); // Will write later
	writeLE<uint16_t>(s, lineAddress.size());

	writeLE<uint32_t>(s, 0); // Will write later
	writeLE<uint16_t>(s, symbolTable.size());

	writeLE<uint32_t>(s, 0); // Will write later
	writeLE<uint16_t>(s, sourceLines.size());

	// lineAddress
	std::streampos lineAddressOffset = s.tellp();
	for (size_t i = 0; i < lineAddress.size(); i++)
	{
		writeLE<uint16_t>(s, lineAddress[i].address);
		writeLE<uint16_t>(s, (lineAddress[i].isCode ? 1 : 0));
		writeLE<uint16_t>(s, lineAddress[i].line);
	}

	// symbolTable
	std::streampos symbolTableOffset = s.tellp();
	for (size_t i = symbolTable.size(); i > 0; i--) // See remarks on symbolTable
	{
		writeString(s, symbolTable[i-1].name);
		writeLE<uint16_t>(s, symbolTable[i-1].type);
		writeLE<uint16_t>(s, symbolTable[i-1].line);
		writeLE<int16_t>(s, symbolTable[i-1].value);
	}

	// sourceLines
	std::streampos sourceLinesOffset = s.tellp();
	for (size_t i = 0; i < sourceLines.size(); i++)
		writeString(s, sourceLines[i]);

	// Write the offsets
	s.seekp(2 + 2 * 256);
	writeLE<uint32_t>(s, lineAddressOffset);

	s.seekp(2 + 2 * 256 + 4 + 2);
	writeLE<uint32_t>(s, symbolTableOffset);

	s.seekp(2 + 2 * 256 + 4 + 2 + 4 + 2);
	writeLE<uint32_t>(s, sourceLinesOffset);
}
