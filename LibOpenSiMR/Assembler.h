/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_ASSEMBLER_H
#define OPENSIMR_ASSEMBLER_H

#include "Program.h"
#include "Parser.h"
#include "Utils/StringChunk.h"
#include <string>
#include <vector>
#include <map>

namespace OpenSiMR
{
	class Assembler
	{
		/// <summary>The lines of the source file to assemble.</summary>
		std::vector<SourceLine> sourceLines;
		/// <summary>Lookup table for constant and label values.</summary>
		std::map<StringChunk, int16_t> symbolTable;

	public:
		Assembler(const StringChunk &code);

		void readConstants();
		void computeAddresses();
		void generateCode(Program &p) const;
		void generateMetadata(Program &p) const;

	private:
		int16_t decodeSymbol(size_t nLine, const StringChunk &sym) const;
		uint8_t decodeAddress(size_t nLine, const StringChunk &sym) const;
	};
}

#endif // OPENSIMR_ASSEMBLER_H