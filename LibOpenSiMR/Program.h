/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_PROGRAM_H
#define OPENSIMR_PROGRAM_H

#include "Utils/FixedSizeInt.h"
#include <vector>
#include <string>
#include <istream>
#include <ostream>

namespace OpenSiMR
{
	/// <summary>Stores correspondences between source code lines and program counters.</summary>
	struct ProgramLineInfo
	{
		/// <summary>Memory address at the specified source line.</summary>
		uint8_t address;
		/// <summary>true if the line contains an assembly instruction.</summary>
		bool isCode;
		/// <summary>The corresponding source code line (1-based indexing).</summary>
		size_t line;

		ProgramLineInfo(uint8_t address, bool isCode, size_t line)
			: address(address), isCode(isCode), line(line) { }
	};

	/// <summary>Types of symbols for the symbol table.</summary>
	enum ProgramSymbolType
	{
		/// <summary>A label (memory address).</summary>
		ProgramSymbolType_Label = 1,
		/// <summary>A constant (numeric value).</summary>
		ProgramSymbolType_Constant = 2
	};

	/// <summary>A symbols in the program's symbol table, along with his value.</summary>
	struct ProgramSymbol
	{
		/// <summary>The name of the symbol.</summary>
		std::string name;
		/// <summary>The type of the symbol.</summary>
		ProgramSymbolType type;
		/// <summary>The line where the symbol was declared.</summary>
		/// <remarks>
		/// The original SiMR compiler (Posten) doesn't write this field correctly for labels.
		/// Instead, it writes 0x1010 normally, but this value can change sometimes
		/// (seems to change if a '.rw CONSTANT' is used before the label declaration).
		/// </remarks>
		size_t line;
		/// <summary>Label address or constant value.</summary>
		int16_t value;

		ProgramSymbol(const std::string &name, ProgramSymbolType type, size_t line, int16_t value)
			: name(name), type(type), line(line), value(value) { }
	};

	/// <summary>
	/// Stores the initial state of a MR program,
	/// along with information for source-level debugging.
	/// </summary>
	struct Program
	{
		/// <summary>Initial Program Counter value.</summary>
		uint8_t initialPc;
		/// <summary>Initial memory values.</summary>
		int16_t initialMemory[0x100];
		/// <summary>Memory address and program source line matchings.</summary>
		std::vector<ProgramLineInfo> lineAddress;
		/// <summary>Table of symbols (labels and constants) of the program.</summary>
		/// <remarks>
		/// Posten writes this table in reverse.
		/// The read/write methods of this class automatically do the reverse operations.
		/// </remarks>
		std::vector<ProgramSymbol> symbolTable;
		/// <summary>The lines of the source code of the program.</summary>
		std::vector<std::string> sourceLines;

		Program()
			: initialPc(), initialMemory() { }

		/// <summary>Assemble a program from the specified source code.</summary>
		/// <param name="code">The source code of the program (.asm file content).</param>
		Program(const std::string &code);

		/// <summary>Load a Program from the specified stream.</summary>
		/// <remarks>The format used is the same as the original SiMR program.</remarks>
		/// <param name="s">The stream from which the program should be read.</param>
		void readFrom(std::istream &s);

		/// <summary>Load a Program from the specified stream.</summary>
		/// <remarks>The format used is the same as the original SiMR program.</remarks>
		/// <param name="s">The stream to which the program should be written.</param>
		void writeTo(std::ostream &s);
	};
}

#endif // OPENSIMR_PROGRAM_H
