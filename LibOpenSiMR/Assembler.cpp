/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Lexer.h"
#include "Parser.h"
#include "Assembler.h"
#include "Instruction.h"
#include "Utils/StrFmt.h"
#include "Utils/FixedSizeInt.h"
#include <stdexcept>
#include <sstream>
#include <iomanip>

 /// <summary>Thrown when an error happens while assembling a MR program.</summary>
class AssemblerException : public std::runtime_error
{
	static std::string makeErrorMessage(size_t nLine, const std::string &message)
	{
		if (nLine != -1)
			return StrFmt() << "MR Assembler - Line " << nLine << ": " << message;
		else
			return StrFmt() << "MR Assembler - " << message;
	}

public:
	/// <summary>Create a new MR assembler exception.</summary>
	/// <param name="nLine">
	/// The line where the error happened,
	/// or -1 for errors in no particular line.
	/// </param>
	/// <param name="message">The error message.</param>
	AssemblerException(size_t nLine, const std::string &message)
		: std::runtime_error(makeErrorMessage(nLine, message))
	{
	}
};

/// <summary>Attempt to parse an integer in the MR language.</summary>
/// <param name="str">The string representing an integer.</param>
/// <param name="result">Receives the converted integer if the operation is successful.</param>
/// <returns>true if the string has been parsed correctly, false otherwise.</returns>
static bool tryParseInteger(const StringChunk &str, int &result)
{
	size_t pos = 0;

	// Parse leading sign(s)
	int sign = 1;
	while (pos < str.length() && (str[pos] == '+' || str[pos] == '-'))
	{
		if (str[pos] == '-')
			sign = -sign;
		pos++;
	}

	// Parse absolute value
	int module;
	if (pos + 2 < str.length() && str.substr(pos, 2) == "0x")
	{
		std::istringstream ss(str.substr(pos + 2).toStdString());
		ss >> std::hex >> module;
		if (!ss || !ss.eof())
			return false;
	}
	else
	{
		std::istringstream ss(str.substr(pos).toStdString());
		ss >> module;
		if (!ss || !ss.eof())
			return false;
	}

	result = sign * module;
	return true;
}

/// <summary>
/// Fits an integer to the specified number of bits if it's in an acceptable range.
/// The purpose of this function is to allow, say, both 16-bit integers in the
/// signed range (-32768 to 32767) and the unsigned range (0 to 65535).
/// This function converts the unsigned range to the signed range
/// (e.g. 40000 -> -25536, which represent the same 16-bit number).
/// </summary>
/// <param name="number">The number to fit in the specified range.</param>
/// <param name="nBits">The desired number of bits for the range.</param>
/// <returns>true if the number can be fit correctly, false otherwise.</returns>
static bool fixTwoComplementRange(int &number, int nBits)
{
	// Convert from valid unsigned range to signed range
	if (number >= (1 << (nBits - 1)) && number < (1 << nBits))
		number -= (1 << nBits);

	// Check if result is in accepted range
	if (number < -(1 << (nBits - 1)) || number >= (1 << (nBits - 1)))
		return false;

	return true;
}

static std::string stringReplace(std::string str, const std::string &from, const std::string &to)
{
	size_t pos = 0;
	while ((pos = str.find(from, pos)) != std::string::npos)
	{
		str.replace(pos, from.length(), to);
		pos += to.length();
	}
	return str;
}

/**************
 * STRUCTURES *
 **************/
static std::map<StringChunk, bool> makeAtomTypesMap()
{
	std::map<StringChunk, bool> map;
	map[".dw"] = false;
	map[".rw"] = false;

	map[".end"] = true; // (This is a HALT branch)

	map["load"] = true;

	map["store"] = true;

	map["br"] = true;
	map["beq"] = true;
	map["bl"] = true;
	map["ble"] = true;
	map["bne"] = true;
	map["bge"] = true;
	map["bg"] = true;

	map["add"] = true;
	map["sub"] = true;
	map["asr"] = true;
	map["and"] = true;
	map["addi"] = true;
	map["subi"] = true;
	return map;
}
/// <summary>
/// List of all accepted atom types.
/// The value specifies if the atom corresponds to a CPU instruction.
/// </summary>
static const std::map<StringChunk, bool> atomTypes = makeAtomTypesMap();

static std::map<StringChunk, OpenSiMR::JumpOpcode> makeJumpOpcodesMap()
{
	std::map<StringChunk, OpenSiMR::JumpOpcode> map;
	map["br"] = OpenSiMR::JumpOpcode_BR;
	map["beq"] = OpenSiMR::JumpOpcode_BEQ;
	map["bl"] = OpenSiMR::JumpOpcode_BL;
	map["ble"] = OpenSiMR::JumpOpcode_BLE;
	map["bne"] = OpenSiMR::JumpOpcode_BNE;
	map["bge"] = OpenSiMR::JumpOpcode_BGE;
	map["bg"] = OpenSiMR::JumpOpcode_BG;
	return map;
}
/// <summary>Correspondences between jump instruction names and jump opcodes.</summary>
static const std::map<StringChunk, OpenSiMR::JumpOpcode> jumpOpcodes = makeJumpOpcodesMap();

static std::map<StringChunk, OpenSiMR::AluOpcode> makeAluOpcodesMap()
{
	std::map<StringChunk, OpenSiMR::AluOpcode> map;
	map["add"] = OpenSiMR::AluOpcode_ADD;
	map["sub"] = OpenSiMR::AluOpcode_SUB;
	map["asr"] = OpenSiMR::AluOpcode_ASR;
	map["and"] = OpenSiMR::AluOpcode_AND;

	map["addi"] = OpenSiMR::AluOpcode_ADD;
	map["subi"] = OpenSiMR::AluOpcode_SUB;
	return map;
}
/// <summary>Correspondences between ALU instruction names and ALU opcodes.</summary>
static const std::map<StringChunk, OpenSiMR::AluOpcode> aluOpcodes = makeAluOpcodesMap();

static std::map<StringChunk, OpenSiMR::RegisterId> makeRegisterNamesMap()
{
	std::map<StringChunk, OpenSiMR::RegisterId> map;
	map["r0"] = OpenSiMR::RegisterId_R0;
	map["r1"] = OpenSiMR::RegisterId_R1;
	map["r2"] = OpenSiMR::RegisterId_R2;
	map["r3"] = OpenSiMR::RegisterId_R3;
	map["r4"] = OpenSiMR::RegisterId_R4;
	map["r5"] = OpenSiMR::RegisterId_R5;
	map["r6"] = OpenSiMR::RegisterId_R6;
	map["r7"] = OpenSiMR::RegisterId_R7;
	return map;
}
/// <summary>Correspondences between register names and register ids.</summary>
static const std::map<StringChunk, OpenSiMR::RegisterId> registerNames = makeRegisterNamesMap();

static OpenSiMR::RegisterId translateRegName(size_t nLine, const StringChunk &regName)
{
	if (registerNames.find(regName) == registerNames.end())
		throw AssemblerException(nLine, StrFmt() << "Register name '" << regName << "' is invalid.");
	return registerNames.find(regName)->second;
}

OpenSiMR::Assembler::Assembler(const StringChunk &code)
{
	OpenSiMR::Parser parser(code);
	while (!parser.isEnd())
		sourceLines.push_back(parser.parseLine());
}

int16_t OpenSiMR::Assembler::decodeSymbol(size_t nLine, const StringChunk &sym) const
{
	// Try to find by name
	if (symbolTable.find(sym) != symbolTable.end())
		return symbolTable.find(sym)->second;

	// Try to decode as integer
	int value;
	if (tryParseInteger(sym, value))
	{
		if (!fixTwoComplementRange(value, 16))
			throw AssemblerException(nLine, StrFmt() << "Value of symbol '" << sym << "' out of range.");
		return (int16_t)value;
	}

	throw AssemblerException(nLine, StrFmt() << "Can't decode symbol '" << sym << "'.");
}

uint8_t OpenSiMR::Assembler::decodeAddress(size_t nLine, const StringChunk &sym) const
{
	int16_t value = decodeSymbol(nLine, sym);
	if (value < 0 || value >= 0x100)
		throw AssemblerException(nLine, StrFmt() << "Value of symbol '" << sym << "' out of address range.");
	return (uint8_t)value;
}

void OpenSiMR::Assembler::readConstants()
{
	for (size_t i = 0; i < sourceLines.size(); i++)
	{
		// Decode constant values in line
		for (size_t j = 0; j < sourceLines[i].constants.size(); j++)
		{
			Constant &c = sourceLines[i].constants[j];

			// Convert value
			int value;
			if (!tryParseInteger(c.valueStr, value))
				throw AssemblerException(i+1,
					StrFmt() << "Can't parse value of constant '" << c.name << "'.");
			if (!fixTwoComplementRange(value, 16))
				throw AssemblerException(i+1,
					StrFmt() << "Value of constant '" << c.name << "' out of range.");
			c.value = (int16_t) value;

			// Add to symbol table
			if (symbolTable.find(c.name) != symbolTable.end())
				throw AssemblerException(i+1,
					StrFmt() << "Duplicate symbol name '" << c.name << "'.");
			symbolTable[c.name] = c.value;
		}
	}
}

void OpenSiMR::Assembler::computeAddresses()
{
	size_t currentAddress = 0;

	for (size_t i = 0; i < sourceLines.size(); i++)
	{
		// Associate address to current line
		sourceLines[i].address = (uint8_t)currentAddress;

		// Associate address to labels in line
		for (size_t j = 0; j < sourceLines[i].labels.size(); j++)
		{
			Label &l = sourceLines[i].labels[j];
			l.address = (uint8_t)currentAddress;

			if (symbolTable.find(l.name) != symbolTable.end())
				throw AssemblerException(i+1,
					StrFmt() << "Duplicate symbol name '" << l.name << "'.");
			symbolTable[l.name] = l.address;
		}

		// Give address to statements and increase the current address
		for (size_t j = 0; j < sourceLines[i].statements.size(); j++)
		{
			Statement &s = sourceLines[i].statements[j];
			s.address = (uint8_t)currentAddress;

			// Advance address depending on instruction
			if (s.op == ".rw")
			{
				int16_t nWords = decodeSymbol(i+1, s.args[0]);
				if (nWords < 0)
					throw AssemblerException(i+1,
						StrFmt() << "Symbol '" << s.args[0] << "' with negative value used in .rw.");
				currentAddress += nWords;
			}
			else if (s.op == ".dw")
			{
				currentAddress += s.args.size();
			}
			else if (s.op == ".begin")
			{
				currentAddress += 0;
			}
			else
			{
				currentAddress++;
			}

			if (currentAddress > 0x100)
				throw AssemblerException(i+1, "Program is too big.");
		}
	}
}

void OpenSiMR::Assembler::generateCode(OpenSiMR::Program &p) const
{
	bool entryPointDeclared = false;

	for (size_t i = 0; i < sourceLines.size(); i++)
	{
		for (size_t j = 0; j < sourceLines[i].statements.size(); j++)
		{
			const Statement &s = sourceLines[i].statements[j];
			if (s.op == ".dw")
			{
				for (size_t k = 0; k < s.args.size(); k++)
					p.initialMemory[s.address+k] = decodeSymbol(i+1, s.args[k]);
			}
			else if (s.op == ".rw")
			{
				int16_t nWords = decodeSymbol(i+1, s.args[0]);
				for (size_t k = 0; k < nWords; k++)
					p.initialMemory[s.address+k] = 0x0000; // Implementation detail
			}
			else if (s.op == ".begin")
			{
				if (entryPointDeclared)
					throw AssemblerException(i+1, "Redeclaration of .begin.");
				p.initialPc = decodeAddress(i+1, s.args[0]);
				entryPointDeclared = true;
			}
			else if (s.op == ".end")
			{
				JumpInstruction instr = {
					/* opcode */ JumpOpcode_HALT,
					/* offsetReg */ RegisterId_R0,
					/* baseAddress */ 0
				};
				p.initialMemory[s.address] = instr.encode();
			}
			else if (s.op == "load")
			{
				LoadInstruction instr = {
					/* destinationReg */ translateRegName(i+1, s.args[2]),
					/* offsetReg */ translateRegName(i+1, s.args[1]),
					/* baseAddress */ decodeAddress(i+1, s.args[0])
				};
				p.initialMemory[s.address] = instr.encode();
			}
			else if (s.op == "store")
			{
				StoreInstruction instr = {
					/* sourceReg */ translateRegName(i+1, s.args[0]),
					/* offsetReg */ translateRegName(i+1, s.args[2]),
					/* baseAddress */ decodeAddress(i+1, s.args[1])
				};
				p.initialMemory[s.address] = instr.encode();
			}
			else if (s.op == "br" ||
					 s.op == "beq" ||
					 s.op == "bl" ||
					 s.op == "ble" ||
					 s.op == "bne" ||
					 s.op == "bge" ||
					 s.op == "bg")
			{
				JumpInstruction instr = {
					/* opcode */ jumpOpcodes.find(s.op)->second,
					/* offsetReg */ RegisterId_R0,
					/* baseAddress */ decodeAddress(i+1, s.args[0])
				};
				p.initialMemory[s.address] = instr.encode();
			}
			else if (s.op == "add" || s.op == "sub" || s.op == "and")
			{
				AluRegInstruction instr = {
					/* opcode */ aluOpcodes.find(s.op)->second,
					/* destinationReg */ translateRegName(i+1, s.args[2]),
					/* source1Reg */ translateRegName(i+1, s.args[0]),
					/* source2Reg */ translateRegName(i+1, s.args[1])
				};
				p.initialMemory[s.address] = instr.encode();
			}
			else if (s.op == "asr")
			{
				AluRegInstruction instr = {
					/* opcode */ AluOpcode_ASR,
					/* destinationReg */ translateRegName(i+1, s.args[1]),
					/* source1Reg */ RegisterId_R0,
					/* source2Reg */ translateRegName(i+1, s.args[0]),
				};
				p.initialMemory[s.address] = instr.encode();
			}
			else if (s.op == "addi" || s.op == "subi")
			{
				int immediate = decodeSymbol(i+1, s.args[1]);
				if (!fixTwoComplementRange(immediate, 5))
					throw AssemblerException(i+1, "ALU immediate out of range.");

				AluImmInstruction instr = {
					/* opcode */ aluOpcodes.find(s.op)->second,
					/* destinationReg */ translateRegName(i+1, s.args[2]),
					/* sourceReg */ translateRegName(i+1, s.args[0]),
					/* immediate */ { immediate },
				};
				p.initialMemory[s.address] = instr.encode();
			}
		}
	}

	if (!entryPointDeclared)
		throw AssemblerException(-1, ".begin not declared.");
}

void OpenSiMR::Assembler::generateMetadata(Program &p) const
{
	for (size_t i = 0; i < sourceLines.size(); i++)
	{
		for (size_t j = 0; j < sourceLines[i].statements.size(); j++)
		{
			const Statement &s = sourceLines[i].statements[j];
			if (s.op == ".begin")
				continue;
			p.lineAddress.push_back(ProgramLineInfo(
				/* address */ s.address,
				/* isCode */ atomTypes.find(s.op)->second,
				/* line */ i+1
			));
		}
	}

	for (size_t i = 0; i < sourceLines.size(); i++)
	{
		for (size_t j = 0; j < sourceLines[i].labels.size(); j++)
		{
			const Label &l = sourceLines[i].labels[j];
			p.symbolTable.push_back(ProgramSymbol(
				/* name */ l.name.toStdString(),
				/* type */ProgramSymbolType_Label,
				/* line */ 0x1010, // for binary perfection. see remarks
				/* value */ l.address
			));
		}

		for (size_t j = 0; j < sourceLines[i].constants.size(); j++)
		{
			const Constant &c = sourceLines[i].constants[j];
			p.symbolTable.push_back(ProgramSymbol(
				/* name */ c.name.toStdString(),
				/* type */ ProgramSymbolType_Constant,
				/* line */ i+1,
				/* value */ c.value
			));
		}
	}

	for (size_t i = 0; i < sourceLines.size(); i++)
	{
		// TODO more binary perfection
		std::string processedLine = StrFmt() <<
				std::hex << std::setw(2) << std::setfill('0') << std::uppercase <<  (int)sourceLines[i].address <<
				": " << stringReplace(stringReplace(sourceLines[i].line.toStdString(), "\t", "        "), "\r", "");
		p.sourceLines.push_back(processedLine);
	}
}
