/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_MACHINE_H
#define OPENSIMR_MACHINE_H

#include "Program.h"
#include "Utils/FixedSizeInt.h"

namespace OpenSiMR
{
	/// <summary>A generic value which remembers read and write operations.</summary>
	template<typename T>
	class Watchable
	{
		/// <summary>true if a write operation has happened since the last clear.</summary>
		bool writeFlag;
		/// <summary>Current value.</summary>
		T value;

	public:
		/// <summary>Create a new Watchable.</summary>
		Watchable()
			: writeFlag(false), value()
		{
		}

		/// <summary>Clear all write operations on this instance.</summary>
		void clear()
		{
			writeFlag = false;
		}

		/// <summary>Checks if a write operation has happened since the last clear.</summary>
		/// <returns>true if a write operation has happened since the last clear.</summary>
		bool isWritten()
		{
			return writeFlag;
		}

		T debugRead()
		{
			return value;
		}

		/// <summary>Reads the value on this instance. Sets the read flag.</summary>
		/// <returns>The value on this instance.</returns>
		operator T()
		{
			return value;
		}

		/// <summary>Reads the value on this watchable. Sets the write flag.</summary>
		Watchable<T> operator =(T newValue)
		{
			writeFlag = true;
			value = newValue;
			return *this;
		}
	};
	/*
	 * Things worth nothing about the OpenSiMR implementation:
	 * - This is an implementation of the machine as in SiMR 2.0.
	 *   This is both because of personal and technical convenience.
	 *
	 *   The most important difference is that SiMR 3+ includes an
	 *   overflow flag, but I found it to be somewhat buggy,
	 *   e.g. on ASR instructions, on overflow flag handling, etc.
	 *
	 *   On the other hand, SiMR 2.0 is simple and the reference
	 *   implementation seems to work correctly.
	 *
	 * - The implementation of the machine is exactly equal to the SiMR 2.0
	 *   implementation circuit, except for an additional 'halt' state to
	 *   handle the end of the program more gracefully.
	 *
	 * - Undocumented or hidden features of MR:
	 *   * The instruction 0xA000 (branch, type 4, address zero) is used
	 *     to signal the end of the program, i.e. on the .end directive.
	 *     This puts the machine in the 'halt' state.
	 *
	 *   * The branch instruction actually accepts an offset register on
	 *     the 3 "zero" bits. Just follow the circuit diagram closely!
	 */

	/// <summary>Possible states of the MR machine.</summary>
	enum MachineState
	{
		/// <summary>Loading an instruction from memory.</summary>
		MachineState_Fetch,
		/// <summary>Dispatching a loaded instruction.</summary>
		MachineState_Decode,
		/// <summary>Doing a memory read.</summary>
		MachineState_Load,
		/// <summary>Doing a memory write.</summary>
		MachineState_Store,
		/// <summary>Doing an ALU operation and fetching.</summary>
		MachineState_AluAndFetch,
		/// <summary>Doing a branch operation and fetching.</summary>
		MachineState_BranchAndFetch,
		/// <summary>Reached the end of the program.</summary>
		MachineState_Halt
	};

	/// <summary>Returns a string representing the current machine state.</summary>
	/// <returns>A human-readable representation of the given machine state.</returns>
	const char *getMachineStateString(MachineState state);

	/// <summary>MR machine simulator.</summary>
	class Machine
	{
	public:
		/// <summary>Machine memory (0x100 words).</summary>
		Watchable<int16_t> memory[0x100];
		/// <summary>General purpose registers R0-R7.</summary>
		Watchable<int16_t> registers[8];

		/// <summary>Current state of the machine.</summary>
		Watchable<MachineState> state;
		/// <summary>Program counter register.</summary>
		Watchable<uint8_t> pc;
		/// <summary>
		/// Saves the memory address specified in an instruction.
		/// This is used for loads, stores and branches.
		/// </summary>
		Watchable<uint8_t> raddr;
		/// <summary>Instruction register (Value of the instruction being executed).</summary>
		Watchable<int16_t> ir;
		/// <summary>Accumulator register before the ALU.</summary>
		Watchable<int16_t> ra;
		/// <summary>Set if the result of the last load/ALU operation was negative.</summary>
		Watchable<bool> rz;
		/// <summary>Set if the result of the last load/ALU operation was zero.</summary>
		Watchable<bool> rn;

	public:
		/// <summary>Create an empty Machine.</summary>
		Machine();
		/// <summary>Set the machine to its initial status.</summary>
		void reset();
		/// <summary>Load the specified program to the machine.</summary>
		/// <param name="program">The program to load.</param>
		void loadProgram(const Program &program);

		/// <summary>Executes a cycle (a clock tick) on the machine.</summary>
		void stepCycle();
		/// <summary>Advances an instruction in the machine.</summary>
		void stepInstruction();
	};
}

#endif // OPENSIMR_MACHINE_H
