/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Machine.h"

const char *OpenSiMR::getMachineStateString(OpenSiMR::MachineState state)
{
	switch (state)
	{
		case MachineState_Fetch:          return "Fetch";
		case MachineState_Decode:         return "Decode";
		case MachineState_Load:           return "Load";
		case MachineState_Store:          return "Store";
		case MachineState_AluAndFetch:    return "ALU & Fetch";
		case MachineState_BranchAndFetch: return "Branch & Fetch";
		case MachineState_Halt:           return "HALT";
		default:                          return NULL;
	}
}

OpenSiMR::Machine::Machine()
{
	reset();
}

void OpenSiMR::Machine::reset()
{
	for (size_t i = 0; i < 0x100; i++)
		memory[i] = 0;
	for (size_t i = 0; i < 8; i++)
		registers[i] = 0;
	state = MachineState_Halt;
	pc = 0x00;
	raddr = 0x00;
	ir = 0x0000;
	ra = 0;
	rz = false;
	rn = false;
}

void OpenSiMR::Machine::loadProgram(const Program &program)
{
	reset();
	for (size_t i = 0; i < 0x100; i++)
		memory[i] = program.initialMemory[i];
	pc = program.initialPc;
	state = MachineState_Fetch;
}

/// <summary>Executes a cycle (a clock tick) on the machine.</summary>
void OpenSiMR::Machine::stepCycle()
{
	/* This apparently senseless piece of code simulates the machine
	 * internal wiring. For more information, see the SiMR diagram. */

	bool Ld_IR = state == MachineState_Fetch ||
				 state == MachineState_AluAndFetch ||
				 state == MachineState_BranchAndFetch;
	bool Ld_PC = state == MachineState_Fetch ||
				 state == MachineState_AluAndFetch ||
				 state == MachineState_BranchAndFetch;
	bool Ld_RAddr = state == MachineState_Decode;
	bool Ld_RA = state == MachineState_Decode;
	bool Ld_RZ = state == MachineState_AluAndFetch ||
				 state == MachineState_Load;
	bool Ld_RN = state == MachineState_AluAndFetch ||
				 state == MachineState_Load;
	bool ERd = state == MachineState_AluAndFetch ||
			   state == MachineState_Load;
	bool LE = state == MachineState_Store;
	size_t PCorAddr = (state == MachineState_Load ||
					state == MachineState_Store ||
					state == MachineState_BranchAndFetch) ? 1 : 0;
	size_t CRf = 0;
	if (state == MachineState_Decode)
		CRf = 1;
	if (state == MachineState_AluAndFetch)
		CRf = 2;
	bool OPERAR = state == MachineState_AluAndFetch;

	// SIMULATE MACHINE WIRING

	// Register read operation
	size_t SELREG[4] = { (size_t)(ir >> 11) & 0x7,
	                     (size_t)(ir >> 8) & 0x7,
	                     (size_t)(ir >> 5) & 0x7,
	                     0 };
	size_t SL = SELREG[CRf];
	int16_t Dout = registers[SL];

	// Memory
	int16_t Min = Dout;
	uint8_t SELADR[2] = { pc, raddr };
	uint8_t SELADR_Output = SELADR[PCorAddr];
	size_t MAddr = SELADR_Output;
	int16_t Mout = memory[MAddr];

	// ALU
	struct Int5 { signed int value : 5; } EXT = { (ir >> 3) & 0x1F };
	int16_t SELDAT[4] = { Mout, Mout, (int16_t)EXT.value, Dout };
	size_t SELDAT_Choice = ((ir >> 2) & 0x1) + (OPERAR ? 0x2 : 0);

	int16_t A = ra;
	int16_t B = SELDAT[SELDAT_Choice];

	int16_t O = B;
	if (OPERAR)
	{
		switch (ir & 0x3)
		{
			case 0: O = (int16_t)(A + B); break;
			case 1: O = (int16_t)(A - B); break;
			case 2: O = (int16_t)(B >> 1); break;
			case 3: O = (int16_t)(A & B); break;
		}
	}

	// Register write
	int16_t Din = O;
	size_t SE = (ir >> 11) & 0x7;

	// Condition evaluation
	bool cond = false, halt = false;
	switch ((ir >> 11) & 0x7)
	{
		case 0: // BEQ
			cond = true;
			break;

		case 1: // BEQ
			cond = rz;
			break;

		case 2: // BL
			cond = rn;
			break;

		case 3: // BLE
			cond = rz && rn;
			break;

		case 4: // HALT
			halt = true;
			break;

		case 5: // BNE
			cond = !rz;
			break;

		case 6: // BGE
			cond = !rn;
			break;

		case 7: // BG
			cond = !rn && !rz;
			break;
	};

	uint8_t type = (ir >> 14) & 0x3;

	// SIMULATE CLOCK TICK (REGISTER STORAGE)
	if (Ld_RAddr)
	{
		raddr = (uint8_t)((uint8_t)Dout + (uint8_t)ir);
	}
	if (Ld_RA)
	{
		ra = Dout;
	}
	if (Ld_PC)
	{
		pc = (uint8_t)(SELADR_Output + 1);
	}
	if (LE)
	{
		memory[MAddr] = Min;
	}
	if (Ld_IR)
	{
		ir = (uint16_t)Mout;
	}
	if (Ld_RZ)
	{
		rz = (O == 0);
	}
	if (Ld_RN)
	{
		rn = (O < 0);
	}
	if (ERd && SE != 0)
	{
		registers[SE] = Din;
	}

	switch (state)
	{
		case MachineState_Fetch:
		case MachineState_AluAndFetch:
		case MachineState_BranchAndFetch:
			state = MachineState_Decode;
			break;

		case MachineState_Decode:
			if (type == 0)
				state = MachineState_Load;
			else if (type == 1)
				state = MachineState_Store;
			else if (type == 2 && halt)
				state = MachineState_Halt;
			else if (type == 2 && !cond)
				state = MachineState_Fetch;
			else if (type == 2 && cond)
				state = MachineState_BranchAndFetch;
			else if (type == 3)
				state = MachineState_AluAndFetch;
			break;

		case MachineState_Load:
		case MachineState_Store:
			state = MachineState_Fetch;
			break;

		case MachineState_Halt:
			state = MachineState_Halt;
			break;
	}
}

void OpenSiMR::Machine::stepInstruction()
{
	/* We consider that the machine has completed an instruction when it is
	 * in the Decode state (and also if the machine is halted).
	 * (To be fair it would make more sense if it was the Fetch state,
	 *  but states such as AluAndFetch do both an operation and a Fetch,
	 *  so we can't both be in Fetch and have the instruction completed.
	 *  On the other hand Decode is common to all instructions.)
	 *
	 * There's an edge case we want to handle that is that if we're in the
	 * Fetch state, then we want to skip the next decode, which corresponds
	 * to the instruction just fetched. The first part does this. */
	if (state == MachineState_Fetch)
		stepCycle();

	do
	{
		stepCycle();
	} while (state != MachineState_Decode && state != MachineState_Halt);
}
