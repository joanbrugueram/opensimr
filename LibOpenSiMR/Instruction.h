/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_INSTRUCTION_H
#define OPENSIMR_INSTRUCTION_H

#include "Utils/FixedSizeInt.h"

namespace OpenSiMR
{
	/// <summary>MR General purpose register identifiers.</summary>
	enum RegisterId
	{
		/// <summary>General purpose register 0 (always 0, writes are ignored).</summary>
		RegisterId_R0,
		/// <summary>General purpose register 1.</summary>
		RegisterId_R1,
		/// <summary>General purpose register 2.</summary>
		RegisterId_R2,
		/// <summary>General purpose register 3.</summary>
		RegisterId_R3,
		/// <summary>General purpose register 4.</summary>
		RegisterId_R4,
		/// <summary>General purpose register 5.</summary>
		RegisterId_R5,
		/// <summary>General purpose register 6.</summary>
		RegisterId_R6,
		/// <summary>General purpose register 7.</summary>
		RegisterId_R7
	};

	/// <summary>A memory load instruction.</summary>
	struct LoadInstruction
	{
		/// <summary>Register where the contents should be loaded.</summary>
		RegisterId destinationReg;
		/// <summary>Register which contains the offset of the memory address.</summary>
		RegisterId offsetReg;
		/// <summary>Base address of the memory address.</summary>
		uint8_t baseAddress;

		/// <summary>Convert this instruction to its memory representation</summary>
		/// <returns>The memory representation of this instruction.</returns>
		uint16_t encode()
		{
			return (uint16_t)((0 << 14) |
				((int)destinationReg << 11) |
				((int)offsetReg << 8) |
				(baseAddress));
		}
	};

	/// <summary>A memory store instruction.</summary>
	struct StoreInstruction
	{
		/// <summary>Register from which to get the value to store.</summary>
		RegisterId sourceReg;
		/// <summary>Register which contains the offset of the memory address.</summary>
		RegisterId offsetReg;
		/// <summary>Base address of the memory address.</summary>
		uint8_t baseAddress;

		/// <summary>Convert this instruction to its memory representation</summary>
		/// <returns>The memory representation of this instruction.</returns>
		uint16_t encode()
		{
			return (uint16_t)((1 << 14) |
				((int)sourceReg << 11) |
				((int)offsetReg << 8) |
				(baseAddress));
		}
	};

	/// <summary>Jump instruction opcodes.</summary>
	enum JumpOpcode
	{
		/// <summary>Branch always.</summary>
		JumpOpcode_BR,
		/// <summary>Branch equal (if Z=1).</summary>
		JumpOpcode_BEQ,
		/// <summary>Branch less (if N=1).</summary>
		JumpOpcode_BL,
		/// <summary>Branch less or equal (if N=1 or Z=1).</summary>
		JumpOpcode_BLE,
		/// <summary>Signals the end of the program.</summary>
		JumpOpcode_HALT,
		/// <summary>Branch not equal (if Z=0).</summary>
		JumpOpcode_BNE,
		/// <summary>Branch greater or equal (if N=0).</summary>
		JumpOpcode_BGE,
		/// <summary>Branch greater (if N=0 and Z=0).</summary>
		JumpOpcode_BG
	};

	/// <summary>A jump instruction.</summary>
	struct JumpInstruction
	{
		/// <summary>Operation code of the jump.</summary>
		JumpOpcode opcode;
		/// <summary>Register which contains the offset of the memory address.</summary>
		/// <remarks>This is undocumented in the original specs.</remarks>
		RegisterId offsetReg;
		/// <summary>Base address of the destination memory address.</summary>
		uint8_t baseAddress;

		/// <summary>Convert this instruction to its memory representation</summary>
		/// <returns>The memory representation of this instruction.</returns>
		uint16_t encode()
		{
			return (uint16_t)((2 << 14) |
				((int)opcode << 11) |
				((int)offsetReg << 8) |
				(baseAddress));
		}
	};

	/// <summary>Arithmetical-logic instruction opcodes.</summary>
	enum AluOpcode
	{
		/// <summary>Add values (A + B).</summary>
		AluOpcode_ADD,
		/// <summary>Substract values (A - B).</summary>
		AluOpcode_SUB,
		/// <summary>Arithmetical right shift by one (A >> 1).</summary>
		AluOpcode_ASR,
		/// <summary>Bitwise and (A and B).</summary>
		AluOpcode_AND
	};

	/// <summary>A arithmetical-logic instruction with two registers as sources.</summary>
	struct AluRegInstruction
	{
		/// <summary>Operation code of the arithmetical-logic instruction.</summary>
		AluOpcode opcode;
		/// <summary>Destination register.</summary>
		RegisterId destinationReg;
		/// <summary>First source register.</summary>
		RegisterId source1Reg;
		/// <summary>Second source register.</summary>
		RegisterId source2Reg;

		/// <summary>Convert this instruction to its memory representation</summary>
		/// <returns>The memory representation of this instruction.</returns>
		uint16_t encode()
		{
			return (uint16_t)((3 << 14) |
				0x04 | ((int)opcode) |
				((int)destinationReg << 11) |
				((int)source1Reg << 8) |
				((int)source2Reg << 5));
		}
	};

	/// <summary>A arithmetical-logic instruction with a register and an immediate as sources.</summary>
	struct AluImmInstruction
	{
		/// <summary>Operation code of the arithmetical-logic instruction.</summary>
		AluOpcode opcode;
		/// <summary>Destination register.</summary>
		RegisterId destinationReg;
		/// <summary>First source register.</summary>
		RegisterId sourceReg;
		/// <summary>Second source immediate.</summary>
		struct Int5 { signed int value : 5; } immediate;

		/// <summary>Convert this instruction to its memory representation</summary>
		uint16_t encode()
		{
			return (uint16_t)((3 << 14) |
				((int)opcode) |
				((int)destinationReg << 11) |
				((int)sourceReg << 8) |
				(immediate.value << 3));
		}
	};
}

#endif
