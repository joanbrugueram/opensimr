/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_PARSER_H
#define OPENSIMR_PARSER_H

#include "Utils/StringChunk.h"
#include "Utils/FixedSizeInt.h"
#include "Lexer.h"
#include <vector>

namespace OpenSiMR
{
	struct Constant
	{
		// Parser
		StringChunk name;
		StringChunk valueStr;
		
		// Emit
		int16_t value;

		Constant(const StringChunk &name, const StringChunk &valueStr)
			: name(name), valueStr(valueStr) { }
	};

	/// <summary>A textual representation of a memory address.</summary>
	struct Label
	{
		// Parser
		StringChunk name;

		// Emit
		uint8_t address;

		Label(const StringChunk &name)
			: name(name) { }
	};

	/// <summary>An instruction or directive which represents one word of the program.</summary>
	struct Statement
	{
		// Parser
		StringChunk op;
		std::vector<StringChunk> args;

		// Emit
		uint8_t address;

		Statement(const StringChunk &op)
			: op(op), args() { }
		Statement &add(const StringChunk &tok) { args.push_back(tok); return *this; }
	};

	/// <summary>Lines of the source code being assembled.</summary>
	struct SourceLine
	{
		// Parser
		StringChunk line;
		std::vector<Label> labels;
		std::vector<Constant> constants;
		std::vector<Statement> statements;

		// Emit
		uint8_t address;

		SourceLine(const StringChunk &line)
			: line(line) { }
	};

	/// <summary>Utility class to split source code into structured tokens.</summary>
	class Parser
	{
		/// <summary>Lexer for the source code.</summary>
		Lexer lex;

	public:
		/// <summary>Create a Parser for the specified source code.</summary>
		/// <param name="code">The source code to parse.</param
		Parser(const StringChunk &code);

		/// <summary>Checks if all the source code has been parsed.</summary>
		/// <returns>true if all the source code has been parsed, false otherwise.</returns>
		bool isEnd() const;

		/// <summary>Parses a line of the source code.</summary>
		/// <returns>A structure containing the tokens of the line, split into categories.</summary>
		SourceLine parseLine();
	};
}

#endif