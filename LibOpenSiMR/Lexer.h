/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENSIMR_LEXER_H
#define OPENSIMR_LEXER_H

#include "Utils/StringChunk.h"

namespace OpenSiMR
{
	/// <summary>Utility class to split an string into tokens.</summary>
	class Lexer
	{
		enum TokenKind
		{
			TokenKind_Separator,
			TokenKind_Word,
			TokenKind_EndOfLine,
			TokenKind_Error
		};

		/// <summary>The string being split into tokens.</summary>
		const StringChunk &str;
		/// <summary>The current position in the string.</summary>
		size_t pos;
		/// <summary>The position at the start of the current line.</summary>
		size_t lineStart;
		/// <summary>The current line number (1-based).</summary>
		size_t nLine;

		TokenKind tokenKind;
		StringChunk token;

		static bool isCommentChar(char c);
		static bool isSeparatorChar(char c);
		static bool isWordChar(char c);

	public:
		/// <summary>Create a new StringTokenizer for the specified string.</summary>
		/// <param name="str">The string to tokenize.</param>
		Lexer(const StringChunk &str);

		/// <summary>Gets the current line.</summary>
		/// <returns>The current line.</summary>
		StringChunk getLine() const;

		/// <summary>Gets the current line number.</summary>
		/// <returns>The current line number.</summary>
		size_t getLineNumber() const;

		/// <summary>Checks if the end of the current string has been reached.</summary>
		/// <returns>true if the end of the current string has been reached, false otherwise.</returns>
		bool isEndOfString() const;

		/// <summary>Checks if the end of the current line or string has been reached.</summary>
		/// <returns>true if the end of the current line or string has been reached, false otherwise.</returns>
		bool isEndOfLine() const;

		/// <summary>Checks if the end of the current line has been reached, and consumes the token.</summary>
		/// <returns>true if the end of the current line has been reached, false otherwise.</returns>
		bool readEndOfLine();

		/// <summary>Read a separator token from the string.</summary>
		/// <returns>The separator token, or null if not available.</returns>
		StringChunk readSeparator();

		/// <summary>Read a word token from the string.</summary>
		/// <returns>The word token, or null if not available.</returns>
		StringChunk readWord();

	private:
		StringChunk consume();
		void nextToken();
	};
}

#endif