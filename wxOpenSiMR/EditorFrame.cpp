/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

// TODO: Enable menus/toolbar when needed

#include "EditorFrame.h"
#include "DebuggerFrame.h"

#include <wx/ffile.h>
#include <wx/artprov.h>
#include <wx/stockitem.h>
#include <wx/gdicmn.h>
#include <wx/filename.h>
#include <wx/msgdlg.h>
#include <wx/filedlg.h>
#include <wx/aboutdlg.h>

#include <string>
#include <fstream>
#include "Machine.h"
#include "Program.h"

static bool ReadTextFile(const wxString &filePath, wxString *content)
{
	wxFFile file;
	return file.Open(filePath, wxT("r")) && file.ReadAll(content);
}

static bool WriteTextFile(const wxString &filePath, const wxString &content)
{
	wxFFile file;
	return file.Open(filePath, wxT("w")) && file.Write(content);
}

enum IDs
{
	ID_PROGRAMRUN,
	ID_BINARYMAKECOD,
	ID_BINARYRUNCOD,
	ID_HELPLICENSE,

	ID_MAINTOOLBAR,
	ID_PROGRAMRUNTOOL,

	ID_EDITORCTRL = 1,
};

static const wxString simrCpuInstructions = wxT("load store br beq bl ble bne bge bg");
static const wxString simrMathInstructions = wxT("add sub asr and addi subi");
static const wxString simrRegisters = wxT("r0 r1 r2 r3 r4 r5 r6 r7 r8");
static const wxString simrDirectives = wxT(".dw .rw .begin .end");

static const wxString sampleProgram = wxString::FromAscii(
".begin collatz\n"
"nombre: .dw 27\n"
"collatz:\n"
"\tload nombre(R0),R1\n"
"\taddi R0,#1,R7\n"
"loop:\n"
"\tsubi R1,#1,R0\n"
"\tbeq end\n"
"\tand R1,R7,R0\n"
"\tbeq parell\n"
"imparell:\n"
"\tadd R1,R1,R2 ; 3n+1\n"
"\tadd R1,R2,R1\n"
"\taddi R1,#1,R1\n"
"\tbr loop\n"
"parell:\n"
"\tasr R1,R1 ; n/2\n"
"\tbr loop\n"
"end:\n"
"\t.end\n"
);

EditorFrame::EditorFrame(wxWindow* parent, wxWindowID id)
	: wxFrame(parent, id, wxEmptyString, wxDefaultPosition, wxSize(500, 500)),
	  FindData(wxFR_DOWN), FindDialog(NULL),
	  filePath(wxEmptyString), isChanged(false)
{
	Connect(wxID_ANY,wxEVT_CLOSE_WINDOW,(wxObjectEventFunction)&EditorFrame::OnClose);

	Connect(wxID_ANY,wxEVT_COMMAND_FIND,(wxObjectEventFunction)&EditorFrame::OnDoFind);
	Connect(wxID_ANY,wxEVT_COMMAND_FIND_NEXT,(wxObjectEventFunction)&EditorFrame::OnDoFindNext);
	Connect(wxID_ANY,wxEVT_COMMAND_FIND_REPLACE,(wxObjectEventFunction)&EditorFrame::OnDoReplace);
	Connect(wxID_ANY,wxEVT_COMMAND_FIND_REPLACE_ALL,(wxObjectEventFunction)&EditorFrame::OnDoReplaceAll);
	Connect(wxID_ANY,wxEVT_COMMAND_FIND_CLOSE,(wxObjectEventFunction)&EditorFrame::OnDoFindClose);

	UpdateTitle();

	// Set up main menu
	MainMenu = new wxMenuBar();
	MenuFile = new wxMenu();
	MenuFileNew = MenuFile->Append(wxID_NEW);
	MenuFileOpen = MenuFile->Append(wxID_OPEN);
	MenuFileSave = MenuFile->Append(wxID_SAVE);
	MenuFileSaveAs = MenuFile->Append(wxID_SAVEAS);
	MenuFile->AppendSeparator();
	MenuFileQuit = MenuFile->Append(wxID_EXIT);
	MainMenu->Append(MenuFile, _("File"));
	MenuEdit = new wxMenu();
	MenuEditUndo = MenuEdit->Append(wxID_UNDO);
	MenuEditRedo = MenuEdit->Append(wxID_REDO);
	MenuEdit->AppendSeparator();
	MenuEditCut = MenuEdit->Append(wxID_CUT);
	MenuEditCopy = MenuEdit->Append(wxID_COPY);
	MenuEditPaste = MenuEdit->Append(wxID_PASTE);
	MenuEdit->AppendSeparator();
	MenuEditSelectAll = MenuEdit->Append(wxID_SELECTALL);
	MenuEdit->AppendSeparator();
	MenuEditFind = MenuEdit->Append(wxID_FIND);
	MenuEditFindAndReplace = MenuEdit->Append(wxID_REPLACE);
	MainMenu->Append(MenuEdit, _("Edit"));
	MenuProgram = new wxMenu();
	MenuProgramRun = MenuProgram->Append(ID_PROGRAMRUN, _("Run...\tF5"));
	MainMenu->Append(MenuProgram, _("Program"));
	MenuBinary = new wxMenu();
	MenuBinaryMakeCod = MenuBinary->Append(ID_BINARYMAKECOD, _("Make COD..."));
	MenuBinaryRunCod = MenuBinary->Append(ID_BINARYRUNCOD, _("Run COD..."));
	MainMenu->Append(MenuBinary, _("Binary"));
	MenuHelp = new wxMenu();
	MenuHelpAbout = MenuHelp->Append(wxID_ABOUT);
	MenuHelpLicense = MenuHelp->Append(ID_HELPLICENSE, _("License..."));
	MainMenu->Append(MenuHelp, _("Help"));
	SetMenuBar(MainMenu);

	Connect(wxID_NEW,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnNew);
	Connect(wxID_OPEN,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnOpen);
	Connect(wxID_SAVE,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnSave);
	Connect(wxID_SAVEAS,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnSaveAs);
	Connect(wxID_EXIT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnQuit);
	Connect(wxID_UNDO,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnUndo);
	Connect(wxID_REDO,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnRedo);
	Connect(wxID_CUT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnCut);
	Connect(wxID_COPY,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnCopy);
	Connect(wxID_PASTE,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnPaste);
	Connect(wxID_SELECTALL,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnSelectAll);
	Connect(wxID_FIND,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnFind);
	Connect(wxID_REPLACE,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnFindAndReplace);
	Connect(ID_PROGRAMRUN,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnRun);
	Connect(ID_BINARYMAKECOD,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnMakeCod);
	Connect(ID_BINARYRUNCOD,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnRunCod);
	Connect(wxID_ABOUT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnAbout);
	Connect(ID_HELPLICENSE,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&EditorFrame::OnLicense);

	// Set up main toolbar
	MainToolbar = new wxToolBar(this, ID_MAINTOOLBAR);
	ToolBarNew = MainToolbar->AddTool(wxID_NEW, wxGetStockLabel(wxID_NEW),
	                                  wxArtProvider::GetBitmap(wxART_NEW, wxART_TOOLBAR));
	ToolBarOpen = MainToolbar->AddTool(wxID_OPEN, wxGetStockLabel(wxID_OPEN),
	                                   wxArtProvider::GetBitmap(wxART_FILE_OPEN, wxART_TOOLBAR));
	ToolBarSave = MainToolbar->AddTool(wxID_SAVE, wxGetStockLabel(wxID_SAVE),
	                                   wxArtProvider::GetBitmap(wxART_FILE_SAVE, wxART_TOOLBAR));
	ToolBarSaveAs = MainToolbar->AddTool(wxID_SAVEAS, wxGetStockLabel(wxID_SAVEAS),
	                                     wxArtProvider::GetBitmap(wxART_FILE_SAVE_AS, wxART_TOOLBAR));
	MainToolbar->AddSeparator();
	ToolBarUndo = MainToolbar->AddTool(wxID_UNDO, wxGetStockLabel(wxID_UNDO),
	                                   wxArtProvider::GetBitmap(wxART_UNDO, wxART_TOOLBAR));
	ToolBarRedo = MainToolbar->AddTool(wxID_REDO, wxGetStockLabel(wxID_REDO),
	                                   wxArtProvider::GetBitmap(wxART_REDO, wxART_TOOLBAR));
	MainToolbar->AddSeparator();
	ToolBarCut = MainToolbar->AddTool(wxID_CUT, wxGetStockLabel(wxID_CUT),
	                                  wxArtProvider::GetBitmap(wxART_CUT, wxART_TOOLBAR));
	ToolBarCopy = MainToolbar->AddTool(wxID_COPY, wxGetStockLabel(wxID_COPY),
	                                   wxArtProvider::GetBitmap(wxART_COPY, wxART_TOOLBAR));
	ToolBarPaste = MainToolbar->AddTool(wxID_PASTE, wxGetStockLabel(wxID_PASTE),
	                                    wxArtProvider::GetBitmap(wxART_PASTE, wxART_TOOLBAR));
	MainToolbar->AddSeparator();
	ToolBarFind = MainToolbar->AddTool(wxID_FIND, wxGetStockLabel(wxID_FIND),
	                                    wxArtProvider::GetBitmap(wxART_FIND, wxART_TOOLBAR));
	ToolBarFindAndReplace = MainToolbar->AddTool(wxID_REPLACE, wxGetStockLabel(wxID_REPLACE),
	                                             wxArtProvider::GetBitmap(wxART_FIND_AND_REPLACE, wxART_TOOLBAR));
	MainToolbar->AddSeparator();
	ToolBarRun = MainToolbar->AddTool(ID_PROGRAMRUNTOOL, _("Run..."),
	                                  wxArtProvider::GetBitmap(wxART_EXECUTABLE_FILE, wxART_TOOLBAR));
	MainToolbar->Realize();
	SetToolBar(MainToolbar);

	Connect(wxID_NEW,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnNew);
	Connect(wxID_OPEN,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnOpen);
	Connect(wxID_SAVE,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnSave);
	Connect(wxID_SAVEAS,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnSaveAs);
	Connect(wxID_CUT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnCut);
	Connect(wxID_COPY,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnCopy);
	Connect(wxID_PASTE,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnPaste);
	Connect(wxID_FIND,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnFind);
	Connect(wxID_REPLACE,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnFindAndReplace);
	Connect(ID_PROGRAMRUNTOOL,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&EditorFrame::OnRun);

	// Set up editor
	EditorCtrl = new wxStyledTextCtrl(this, ID_EDITORCTRL);

	EditorCtrl->SetMarginType(0, wxSTC_MARGIN_NUMBER);
	EditorCtrl->SetMarginWidth(0, 30);

	EditorCtrl->SetLexer(wxSTC_LEX_ASM);
	EditorCtrl->SetKeyWords(0, simrCpuInstructions);
	EditorCtrl->SetKeyWords(1, simrMathInstructions);
	EditorCtrl->SetKeyWords(2, simrRegisters);
	EditorCtrl->SetKeyWords(3, simrDirectives);

	EditorCtrl->StyleSetForeground(wxSTC_STYLE_DEFAULT, *wxBLACK);
	EditorCtrl->StyleSetBackground(wxSTC_STYLE_DEFAULT, *wxWHITE);
	EditorCtrl->StyleSetSize(wxSTC_STYLE_DEFAULT, 11);
	EditorCtrl->StyleSetFaceName(wxSTC_STYLE_DEFAULT, wxT("Monospace"));

	EditorCtrl->StyleClearAll();

	EditorCtrl->StyleSetForeground(wxSTC_ASM_DEFAULT, wxTheColourDatabase->Find(wxT("RED")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_COMMENT, wxTheColourDatabase->Find(wxT("GREY")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_NUMBER, wxTheColourDatabase->Find(wxT("RED")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_STRING, *wxBLACK); // Not in SiMR
	EditorCtrl->StyleSetForeground(wxSTC_ASM_OPERATOR, *wxBLACK); // Not in SiMR
	EditorCtrl->StyleSetForeground(wxSTC_ASM_IDENTIFIER, wxTheColourDatabase->Find(wxT("FOREST GREEN")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_CPUINSTRUCTION, wxTheColourDatabase->Find(wxT("BLUE")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_MATHINSTRUCTION, wxTheColourDatabase->Find(wxT("STEEL BLUE")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_REGISTER, wxTheColourDatabase->Find(wxT("GOLD")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_DIRECTIVE, wxTheColourDatabase->Find(wxT("BROWN")));
	EditorCtrl->StyleSetForeground(wxSTC_ASM_DIRECTIVEOPERAND, *wxBLACK); // Not in SiMR
	EditorCtrl->StyleSetForeground(wxSTC_ASM_COMMENTBLOCK, *wxBLACK); // Not in SiMR
	EditorCtrl->StyleSetForeground(wxSTC_ASM_CHARACTER, *wxBLACK); // Not in SiMR
	EditorCtrl->StyleSetForeground(wxSTC_ASM_STRINGEOL, *wxBLACK); // Not in SiMR
	EditorCtrl->StyleSetForeground(wxSTC_ASM_EXTINSTRUCTION, *wxBLACK); // Not in SiMR

	Connect(ID_EDITORCTRL, wxEVT_STC_SAVEPOINTREACHED, (wxObjectEventFunction)&EditorFrame::OnSavePointReached);
	Connect(ID_EDITORCTRL, wxEVT_STC_SAVEPOINTLEFT, (wxObjectEventFunction)&EditorFrame::OnSavePointLeft);

	EditorCtrl->SetText(sampleProgram);
	EditorCtrl->EmptyUndoBuffer();
	EditorCtrl->SetSavePoint();
	EditorCtrl->SetFocus();
}

EditorFrame::~EditorFrame()
{
}

void EditorFrame::UpdateTitle()
{
	wxString title;

	if (isChanged)
		title += wxT("*");

	if (!filePath.IsEmpty())
		title += wxFileName(filePath).GetFullName();
	else
		title += wxString(_("(untitled)"));

	title += wxT(" - ");
	title += _("OpenSiMR Editor");

	SetTitle(title);
}

void EditorFrame::NewFile()
{
	EditorCtrl->ClearAll();
	EditorCtrl->EmptyUndoBuffer();
	EditorCtrl->SetSavePoint();

	EditorCtrl->GotoPos(0);
	EditorCtrl->SetFocus();

	filePath = wxEmptyString;
	isChanged = false;
	UpdateTitle();
}

void EditorFrame::LoadFromFilePath()
{
	wxString content;
	if (!ReadTextFile(filePath, &content))
	{
		wxMessageBox(
			_("Error reading the specified file."),
			_("Read error"), wxOK | wxICON_ERROR, this);
		return;
	}

	EditorCtrl->ClearAll();
	EditorCtrl->Cancel();
	EditorCtrl->SetUndoCollection(false);

	EditorCtrl->AddText(content);

	EditorCtrl->SetUndoCollection(true);
	EditorCtrl->EmptyUndoBuffer();
	EditorCtrl->SetSavePoint();

	EditorCtrl->GotoPos(0);
	EditorCtrl->SetFocus();

	isChanged = false;
	UpdateTitle();
}

void EditorFrame::SaveToFilePath()
{
	if (!WriteTextFile(filePath, EditorCtrl->GetText()))
	{
		wxMessageBox(
			_("Error saving the specified file."),
			_("Save error"), wxOK | wxICON_ERROR, this);
		return;
	}

	EditorCtrl->SetSavePoint();
}


bool EditorFrame::AskFilePath(int kind)
{
	wxString path = wxFileSelector(
		(kind == wxFD_OPEN) ? _("Open ASM File") : _("Save ASM File"),
		wxEmptyString, wxEmptyString, wxT("asm"),
		_("ASM Files|*.asm|All Files|*.*"),
		(kind == wxFD_OPEN) ? wxFD_OPEN | wxFD_FILE_MUST_EXIST
		                    : wxFD_SAVE | wxFD_OVERWRITE_PROMPT,
		this);
	if (path.IsEmpty())
		return false;

	filePath = path;
	return true;
}

bool EditorFrame::CanClose()
{
	if (isChanged)
	{
		int answer = wxMessageBox(
			_("Save changes before discarding the current file?"),
			_("Confirm"), wxYES_NO | wxCANCEL | wxICON_EXCLAMATION, this);
		if (answer == wxYES)
		{
			if (!AskFilePath(wxFD_SAVE))
				return false;

			SaveToFilePath();
			return true;
		}
		else if (answer == wxNO)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}

void EditorFrame::OnClose(wxCloseEvent& event)
{
	if (event.CanVeto())
	{
		// Ask the user for permission to destroy his work
		if (!CanClose())
		{
			event.Veto();
			return;
		}
	}

	Destroy();
}

int EditorFrame::TranslateSearchFlags(int flags)
{
	int stcFlags = 0;
	if (flags & wxFR_MATCHCASE)
		stcFlags |= wxSTC_FIND_MATCHCASE;
	if (flags & wxFR_WHOLEWORD)
		stcFlags |= wxSTC_FIND_WHOLEWORD;
	return stcFlags;
}

int EditorFrame::SearchToTarget(const wxString &str, int start, int end)
{
	EditorCtrl->SetTargetStart(start);
	EditorCtrl->SetTargetEnd(end);
	return EditorCtrl->SearchInTarget(str);
}

void EditorFrame::SelectTarget()
{
	int lineStart = EditorCtrl->LineFromPosition(EditorCtrl->GetTargetStart());
	int lineEnd = EditorCtrl->LineFromPosition(EditorCtrl->GetTargetEnd());
	for (int l = lineStart; l <= lineEnd; l++)
		EditorCtrl->EnsureVisible(l); // TODO does this work?

	EditorCtrl->SetSelection(EditorCtrl->GetTargetStart(), EditorCtrl->GetTargetEnd());
}

void EditorFrame::DoFind(wxFindDialogEvent &event)
{
	wxString str = event.GetFindString();
	bool down = event.GetFlags() & wxFR_DOWN;

	// Translate and set search flags
	EditorCtrl->SetSearchFlags(TranslateSearchFlags(event.GetFlags()));

	// Search in text
	int pos;
	if (down)
		pos = SearchToTarget(str, EditorCtrl->GetSelectionEnd(), EditorCtrl->GetTextLength());
	else
		pos = SearchToTarget(str, EditorCtrl->GetSelectionStart(), 0);

	if (pos == -1) // Wrap
	{
		if (down)
			pos = SearchToTarget(str, 0, EditorCtrl->GetTextLength());
		else
			pos = SearchToTarget(str, EditorCtrl->GetTextLength(), 0);
	}

	// Select result or show message
	if (pos != -1)
		SelectTarget();
	else
		wxMessageBox(_("The search text was not found."), _("Not found"),
		             wxOK | wxICON_INFORMATION, FindDialog);
}

void EditorFrame::OnDoFind(wxFindDialogEvent &event)
{
	DoFind(event);
}

void EditorFrame::OnDoFindNext(wxFindDialogEvent &event)
{
	DoFind(event);
}

void EditorFrame::OnDoReplace(wxFindDialogEvent &event)
{
	// Is the selected text the search string?
	EditorCtrl->SetSearchFlags(TranslateSearchFlags(event.GetFlags()));
	EditorCtrl->SetTargetStart(EditorCtrl->GetSelectionStart());
	EditorCtrl->SetTargetEnd(EditorCtrl->GetSelectionEnd());
	if (EditorCtrl->SearchInTarget(event.GetFindString()) != -1 &&
		EditorCtrl->GetTargetStart() == EditorCtrl->GetSelectionStart() &&
		EditorCtrl->GetTargetEnd() == EditorCtrl->GetSelectionEnd())
	{
		EditorCtrl->ReplaceSelection(event.GetReplaceString());
	}

	DoFind(event);
}

void EditorFrame::OnDoReplaceAll(wxFindDialogEvent &event)
{
	EditorCtrl->SetSearchFlags(TranslateSearchFlags(event.GetFlags()));
	int posSearch = 0, posMatch;
	while ((posMatch = SearchToTarget(event.GetFindString(),
	                                  posSearch, EditorCtrl->GetTextLength())) != -1)
	{
		EditorCtrl->ReplaceTarget(event.GetReplaceString());
		posSearch = EditorCtrl->GetTargetEnd();
	}

	if (posSearch == 0)
		wxMessageBox(_("The search text was not found."), _("Not found"),
		             wxOK | wxICON_INFORMATION, FindDialog);
}

void EditorFrame::OnDoFindClose(wxFindDialogEvent &event)
{
	FindDialog->Destroy();
	FindDialog = NULL;
}

void EditorFrame::OnNew(wxCommandEvent& event)
{
	if (!CanClose())
		return;

	NewFile();
}

void EditorFrame::OnOpen(wxCommandEvent& event)
{
	if (!CanClose())
		return;

	if (!AskFilePath(wxFD_OPEN))
		return;

	LoadFromFilePath();
}

void EditorFrame::OnSave(wxCommandEvent& event)
{
	if (filePath.IsEmpty() && !AskFilePath(wxFD_SAVE))
		return;

	SaveToFilePath();
}

void EditorFrame::OnSaveAs(wxCommandEvent& event)
{
	if (!AskFilePath(wxFD_SAVE))
		return;

	SaveToFilePath();
}

void EditorFrame::OnQuit(wxCommandEvent& event)
{
	Close();
}

void EditorFrame::OnUndo(wxCommandEvent& event)
{
	EditorCtrl->Undo();
}

void EditorFrame::OnRedo(wxCommandEvent& event)
{
	EditorCtrl->Redo();
}

void EditorFrame::OnCut(wxCommandEvent& event)
{
	EditorCtrl->Cut();
}

void EditorFrame::OnCopy(wxCommandEvent& event)
{
	EditorCtrl->Copy();
}

void EditorFrame::OnPaste(wxCommandEvent& event)
{
	EditorCtrl->Paste();
}

void EditorFrame::OnSelectAll(wxCommandEvent& event)
{
	EditorCtrl->SelectAll();
}

void EditorFrame::OnFind(wxCommandEvent& event)
{
	if (FindDialog != NULL) // Already open
		return;

	FindDialog = new wxFindReplaceDialog(this, &FindData,
	                                     _("Find in Source"));
	FindDialog->Show(true);
}

void EditorFrame::OnFindAndReplace(wxCommandEvent& event)
{
	if (FindDialog != NULL) // Already open
		return;

	FindDialog = new wxFindReplaceDialog(this, &FindData,
	                                     _("Replace in Source"), wxFR_REPLACEDIALOG);
	FindDialog->Show(true);
}

void EditorFrame::OnRun(wxCommandEvent& event)
{
	try
	{
		std::string sourceCode = std::string(EditorCtrl->GetText().mb_str());
		OpenSiMR::Program program(sourceCode);

		DebuggerFrame *dbg = new DebuggerFrame(this, wxID_ANY, program);
		dbg->Show();
	}
	catch (const std::exception &ex)
	{
		wxMessageBox(wxString::FromAscii(ex.what()),
			_("Error"), wxOK | wxICON_ERROR, this);
	}
}

void EditorFrame::OnMakeCod(wxCommandEvent& event)
{
	try
	{
		std::string sourceCode = std::string(EditorCtrl->GetText().mb_str());
		OpenSiMR::Program program(sourceCode);

		wxString path = wxFileSelector(_("Save COD File"),
		                               wxEmptyString, wxEmptyString, wxT("cod"),
		                               _("COD Files|*.cod|All Files|*.*"),
		                               wxFD_SAVE | wxFD_OVERWRITE_PROMPT, this);
		if (!path.IsEmpty())
		{
			std::ofstream out(path.mb_str(), std::ofstream::binary);
			program.writeTo(out);
		}
	}
	catch (const std::exception &ex)
	{
		wxMessageBox(wxString::FromAscii(ex.what()),
			_("Error"), wxOK | wxICON_ERROR, this);
	}
}

void EditorFrame::OnRunCod(wxCommandEvent& event)
{
	try
	{
		wxString path = wxFileSelector(_("Load COD File"),
		                               wxEmptyString, wxEmptyString, wxT("cod"),
		                               _("COD Files|*.cod|All Files|*.*"),
		                               wxFD_OPEN | wxFD_FILE_MUST_EXIST, this);
		if (!path.IsEmpty())
		{
			OpenSiMR::Program program;

			std::ifstream in(path.mb_str(), std::ifstream::binary);
			program.readFrom(in);

			DebuggerFrame *dbg = new DebuggerFrame(this, wxID_ANY, program);
			dbg->Show();
		}
	}
	catch (const std::exception &ex)
	{
		wxMessageBox(wxString::FromAscii(ex.what()),
			_("Error"), wxOK | wxICON_ERROR, this);
	}
}

void EditorFrame::OnAbout(wxCommandEvent& event)
{
	wxAboutDialogInfo info;
	info.SetName(wxT("(wx)OpenSiMR"));
	info.SetDescription(_("A free, open source, cross-platform implementation of the MR machine."));
	info.SetVersion(wxT("1.0"));
	info.AddDeveloper(wxT("Joan Bruguera Mic\xf3"));
	info.SetCopyright(wxT("Copyright (C) 2013 Joan Bruguera Mic\xf3. Licensed under the GPL."));
	wxAboutBox(info);
}

void EditorFrame::OnLicense(wxCommandEvent& event)
{
	wxString license;
	if (ReadTextFile(wxT("COPYING.txt"), &license))
	{
		wxDialog *dlg = new wxDialog(this, wxID_ANY, _("License"));
		new wxTextCtrl(dlg, wxID_ANY, license,
			wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY);
		dlg->ShowModal();
		dlg->Destroy();
	}
}

void EditorFrame::OnSavePointReached(wxStyledTextEvent& event)
{
	isChanged = false;
	UpdateTitle();
}

void EditorFrame::OnSavePointLeft(wxStyledTextEvent& event)
{
	isChanged = true;
	UpdateTitle();
}
