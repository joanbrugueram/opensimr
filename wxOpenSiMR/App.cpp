/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "EditorFrame.h"

#include <wx/app.h>

class MyApp : public wxApp
{
	public:
		virtual bool OnInit();
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	EditorFrame* Frame = new EditorFrame(NULL);
	Frame->Show();
	SetTopWindow(Frame);
	return true;
}

