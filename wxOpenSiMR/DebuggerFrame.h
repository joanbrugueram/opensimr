/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEBUGGERFRAME_H
#define DEBUGGERFRAME_H

#include <wx/listctrl.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/menu.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/listbox.h>
#include <wx/frame.h>

#include "Program.h"
#include "Machine.h"

class DebuggerFrame: public wxFrame
{
	public:
		DebuggerFrame(wxWindow* parent, wxWindowID id, const OpenSiMR::Program &program);
		virtual ~DebuggerFrame();

	private:
		wxMenuBar *MainMenu;
		wxMenu *MenuProgram;
		wxMenuItem *MenuItemProgramReset;
		wxMenuItem *MenuItemProgramRun;
		wxMenuItem *MenuItemProgramStop;
		wxMenuItem *MenuItemProgramStepCycle;
		wxMenuItem *MenuItemProgramStepInstruction;

		wxListBox *ListBoxCode;

		wxStaticText *StaticTextR[8];
		wxTextCtrl *TextCtrlR[8];

		wxCheckBox *CheckBoxRZ;
		wxCheckBox *CheckBoxRN;

		wxStaticText *StaticTextPC;
		wxTextCtrl *TextCtrlPC;
		wxStaticText *StaticTextIR;
		wxTextCtrl *TextCtrlIR;
		wxStaticText *StaticTextState;
		wxTextCtrl *TextCtrlState;
		wxStaticText *StaticTextRaddr;
		wxTextCtrl *TextCtrlRaddr;
		wxStaticText *StaticTextRA;
		wxTextCtrl *TextCtrlRA;

		wxListView *ListViewMemory;

		OpenSiMR::Program program;
		OpenSiMR::Machine machine;

		wxThread *machineThread;

		bool running;

		int selectedCodeLine;

		void UpdateUI();

		void SetRunning(bool run);

		void OnIdle(wxIdleEvent &event);

		void OnReset(wxCommandEvent &event);
		void OnRun(wxCommandEvent &event);
		void OnStop(wxCommandEvent &event);
		void OnStepCycle(wxCommandEvent &event);
		void OnStepInstruction(wxCommandEvent &event);
		void OnListBoxCodeSelect(wxCommandEvent &event);
		void OnCheckBoxRZClick(wxCommandEvent &event);
		void OnCheckBoxRNClick(wxCommandEvent &event);
};

#endif
