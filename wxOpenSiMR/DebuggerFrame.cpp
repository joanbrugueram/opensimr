/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DebuggerFrame.h"

#include <wx/font.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/msgdlg.h>
#include <wx/gdicmn.h>

enum IDs
{
	ID_PROGRAMRESET = 1,
	ID_PROGRAMRUN,
	ID_PROGRAMSTOP,
	ID_PROGRAMSTEPCYCLE,
	ID_PROGRAMSTEPINSTRUCTION,

	ID_LISTBOXCODE,

	ID_TEXTCTRLR0,
	ID_TEXTCTRLR1,
	ID_TEXTCTRLR2,
	ID_TEXTCTRLR3,
	ID_TEXTCTRLR4,
	ID_TEXTCTRLR5,
	ID_TEXTCTRLR6,
	ID_TEXTCTRLR7,
	ID_STATICTEXTR0,
	ID_STATICTEXTR1,
	ID_STATICTEXTR2,
	ID_STATICTEXTR3,
	ID_STATICTEXTR4,
	ID_STATICTEXTR5,
	ID_STATICTEXTR6,
	ID_STATICTEXTR7,

	ID_CHECKBOXRZ,
	ID_CHECKBOXRN,

	ID_STATICTEXTPC,
	ID_TEXTCTRLPC,
	ID_STATICTEXTIR,
	ID_TEXTCTRLIR,
	ID_STATICTEXTSTATE,
	ID_TEXTCTRLSTATE,
	ID_STATICTEXTRADDR,
	ID_TEXTCTRLRADDR,
	ID_STATICTEXTRA,
	ID_TEXTCTRLRA,

	ID_LISTVIEWMEMORY,
};

DebuggerFrame::DebuggerFrame(wxWindow* parent, wxWindowID id, const OpenSiMR::Program &program)
	: wxFrame(parent, id, _("OpenSiMR Debugger")),
	  program(program)
{
	Connect(wxID_ANY,wxEVT_IDLE,(wxObjectEventFunction)&DebuggerFrame::OnIdle);

	/* This is a workaround for the ugly default background color of Windows.
	 * On Windows, just add everything on a wxPanel instead of directly on the window.
	 * See also: http://wiki.wxwidgets.org/WxFAQ */
	#ifdef __WXMSW__
	wxWindow *thisw = new wxPanel(this);
	#else
	wxWindow *thisw = this;
	#endif

	// Set up main menu
	MainMenu = new wxMenuBar();
	MenuProgram = new wxMenu();
	MenuItemProgramReset = MenuProgram->Append(ID_PROGRAMRESET, _("Reset"));
	MenuProgram->AppendSeparator();
	MenuItemProgramRun = MenuProgram->Append(ID_PROGRAMRUN, _("Run\tF5"));
	MenuItemProgramStop = MenuProgram->Append(ID_PROGRAMSTOP, _("Stop\tCTRL+F5"));
	MenuProgram->AppendSeparator();
	MenuItemProgramStepCycle = MenuProgram->Append(ID_PROGRAMSTEPCYCLE, _("Step Cycle\tF7"));
	MenuItemProgramStepInstruction = MenuProgram->Append(ID_PROGRAMSTEPINSTRUCTION, _("Step Instruction\tF8"));
	MainMenu->Append(MenuProgram, _("Program"));
	SetMenuBar(MainMenu);

	Connect(ID_PROGRAMRESET,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&DebuggerFrame::OnReset);
	Connect(ID_PROGRAMRUN,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&DebuggerFrame::OnRun);
	Connect(ID_PROGRAMSTOP,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&DebuggerFrame::OnStop);
	Connect(ID_PROGRAMSTEPCYCLE,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&DebuggerFrame::OnStepCycle);
	Connect(ID_PROGRAMSTEPINSTRUCTION,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&DebuggerFrame::OnStepInstruction);

	// Set up code list
	ListBoxCode = new wxListBox(thisw, ID_LISTBOXCODE, wxDefaultPosition, wxSize(400,-1));
	ListBoxCode->SetFont(wxFont(wxDEFAULT, wxTELETYPE, wxFONTSTYLE_NORMAL, wxNORMAL));

	for (size_t i = 0; i < program.sourceLines.size(); i++)
		ListBoxCode->Append(wxString(program.sourceLines[i].c_str(), wxConvUTF8));

	Connect(ID_LISTBOXCODE,wxEVT_COMMAND_LISTBOX_SELECTED,(wxObjectEventFunction)&DebuggerFrame::OnListBoxCodeSelect);

	// Set up debug info - Main registers
	wxStaticBoxSizer *MainRegistersBox = new wxStaticBoxSizer(wxHORIZONTAL, thisw, _("Main Registers"));
	wxFlexGridSizer *MainRegistersSizer = new wxFlexGridSizer(2 /* columns */);
	for (int i = 0; i < 8; i++)
	{
		// Use wxTE_RICH2 for wxTextCtrl for colors to work in Windows
		StaticTextR[i] = new wxStaticText(thisw, ID_STATICTEXTR0+i, wxString::Format(wxT("R%d:"), i));
		MainRegistersSizer->Add(StaticTextR[i], 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
		TextCtrlR[i] = new wxTextCtrl(thisw, ID_TEXTCTRLR0+i, wxEmptyString,
		                              wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_RICH2);
		MainRegistersSizer->Add(TextCtrlR[i], 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	}
	MainRegistersBox->Add(MainRegistersSizer, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	// Set up debug info - Flags registers
	wxStaticBoxSizer *FlagsRegistersBox = new wxStaticBoxSizer(wxHORIZONTAL, thisw, _("Flags"));

	CheckBoxRZ = new wxCheckBox(thisw, ID_CHECKBOXRZ, _("Z"));
	FlagsRegistersBox->Add(CheckBoxRZ, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	CheckBoxRN = new wxCheckBox(thisw, ID_CHECKBOXRN, _("N"));
	FlagsRegistersBox->Add(CheckBoxRN, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	Connect(ID_CHECKBOXRZ,wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&DebuggerFrame::OnCheckBoxRZClick);
	Connect(ID_CHECKBOXRN,wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&DebuggerFrame::OnCheckBoxRNClick);

	// Set up debug info - Machine registers
	wxStaticBoxSizer *MachineRegistersBox = new wxStaticBoxSizer(wxHORIZONTAL, thisw, _("Machine"));
	wxFlexGridSizer *MachineRegistersSizer = new wxFlexGridSizer(2 /* columns */);

	StaticTextPC = new wxStaticText(thisw, ID_STATICTEXTPC, _("PC:"));
	MachineRegistersSizer->Add(StaticTextPC, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	TextCtrlPC = new wxTextCtrl(thisw, ID_TEXTCTRLPC, wxEmptyString,
	                            wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_RICH2);
	MachineRegistersSizer->Add(TextCtrlPC, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	StaticTextIR = new wxStaticText(thisw, ID_STATICTEXTIR, _("IR:"));
	MachineRegistersSizer->Add(StaticTextIR, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	TextCtrlIR = new wxTextCtrl(thisw, ID_TEXTCTRLIR, wxEmptyString,
	                            wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_RICH2);
	MachineRegistersSizer->Add(TextCtrlIR, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	StaticTextState = new wxStaticText(thisw, ID_STATICTEXTSTATE, _("St:"));
	MachineRegistersSizer->Add(StaticTextState, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	TextCtrlState = new wxTextCtrl(thisw, ID_TEXTCTRLSTATE, wxEmptyString,
	                            wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_RICH2);
	MachineRegistersSizer->Add(TextCtrlState, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	StaticTextRaddr = new wxStaticText(thisw, ID_STATICTEXTRADDR, _("R@:"));
	MachineRegistersSizer->Add(StaticTextRaddr, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	TextCtrlRaddr = new wxTextCtrl(thisw, ID_TEXTCTRLRADDR, wxEmptyString,
	                            wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_RICH2);
	MachineRegistersSizer->Add(TextCtrlRaddr, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	StaticTextRA = new wxStaticText(thisw, ID_STATICTEXTRA, _("RA:"));
	MachineRegistersSizer->Add(StaticTextRA, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
	TextCtrlRA = new wxTextCtrl(thisw, ID_TEXTCTRLRA, wxEmptyString,
	                            wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_RICH2);
	MachineRegistersSizer->Add(TextCtrlRA, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	MachineRegistersBox->Add(MachineRegistersSizer, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	// Set up debug info
	wxBoxSizer *DebugInfoSizer = new wxBoxSizer(wxVERTICAL);
	DebugInfoSizer->Add(MainRegistersBox, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	DebugInfoSizer->Add(FlagsRegistersBox, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	DebugInfoSizer->Add(MachineRegistersBox, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);

	// Set up memory list
	ListViewMemory = new wxListView(thisw, ID_LISTVIEWMEMORY, wxDefaultPosition, wxSize(200,-1), wxLC_REPORT, wxDefaultValidator, _T("ID_LISTVIEWMEMORY"));

	ListViewMemory->InsertColumn(0, _("Address"));
	ListViewMemory->InsertColumn(1, _("Content"));
	for (size_t i = 0; i < 0x100; i++)
		ListViewMemory->InsertItem(i, wxString::Format(wxT("%.2X"), (int)i));

	// Set up main sizer
	wxBoxSizer *MainSizer = new wxBoxSizer(wxHORIZONTAL);
	MainSizer->Add(ListBoxCode, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	MainSizer->Add(DebugInfoSizer, 0, wxALL|wxALIGN_TOP|wxALIGN_CENTER_HORIZONTAL, 5);
	MainSizer->Add(ListViewMemory, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	thisw->SetSizer(MainSizer);
	MainSizer->Fit(thisw);
	MainSizer->SetSizeHints(thisw);

	#ifdef __WXMSW__
	this->Fit(); // See above
	#endif

	// Set up machine
	machine.loadProgram(program);
	SetRunning(false);
	UpdateUI();
}

DebuggerFrame::~DebuggerFrame()
{
}

void DebuggerFrame::UpdateUI()
{
	wxColour noClr = *wxBLACK;
	wxColour wrClr = *wxRED;

	for (size_t i = 0; i < 0x100; i++)
	{
		ListViewMemory->SetItemTextColour(i, machine.memory[i].isWritten() ? wrClr : noClr);
		ListViewMemory->SetItem(i, 1, wxString::Format(wxT("%.4hX"), machine.memory[i].debugRead()));
		machine.memory[i].clear();
	}

	for (size_t reg = 0; reg < 8; reg++)
	{
		TextCtrlR[reg]->SetForegroundColour(machine.registers[reg].isWritten() ? wrClr : noClr);
		TextCtrlR[reg]->SetValue(wxString::Format(wxT("%.4hX"), machine.registers[reg].debugRead()));
		machine.registers[reg].clear();
	}

	CheckBoxRZ->SetForegroundColour(machine.rz.isWritten() ? wrClr : noClr);
	CheckBoxRZ->SetValue(machine.rz.debugRead());
	machine.rz.clear();

	CheckBoxRN->SetForegroundColour(machine.rn.isWritten() ? wrClr : noClr);
	CheckBoxRN->SetValue(machine.rn.debugRead());
	machine.rn.clear();

	TextCtrlPC->SetForegroundColour(machine.pc.isWritten() ? wrClr : noClr);
	TextCtrlPC->SetValue(wxString::Format(wxT("%.2hhX"), machine.pc.debugRead()));
	machine.pc.clear();

	TextCtrlIR->SetForegroundColour(machine.ir.isWritten() ? wrClr : noClr);
	TextCtrlIR->SetValue(wxString::Format(wxT("%.4hX"), machine.ir.debugRead()));
	machine.ir.clear();

	TextCtrlState->SetForegroundColour(machine.state.isWritten() ? wrClr : noClr);
	TextCtrlState->SetValue(wxString::FromAscii(OpenSiMR::getMachineStateString(machine.state.debugRead())));
	machine.state.clear();

	TextCtrlRaddr->SetForegroundColour(machine.raddr.isWritten() ? wrClr : noClr);
	TextCtrlRaddr->SetValue(wxString::Format(wxT("%.4hX"), machine.raddr.debugRead()));
	machine.raddr.clear();

	TextCtrlRA->SetForegroundColour(machine.ra.isWritten() ? wrClr : noClr);
	TextCtrlRA->SetValue(wxString::Format(wxT("%.4hX"), machine.ra.debugRead()));
	machine.ra.clear();

	// Update selected line
	/* Update the currently selected code line.
	*
	* We could just select the line corresponding to the PC, but this
	* would be very unintuitive, because when we're (say) in the Decode state,
	* PC points not to the instruction being decoded, but the next one!
	*
	* So unless the State is fetch (in which the PC actually points to the
	* instruction which is to be fetched, since it hasn't been incremented yet),
	* we just select the line corresponding to the previous PC.
	*/
	int findPC = machine.pc.debugRead();
	if (machine.state.debugRead() != OpenSiMR::MachineState_Fetch)
		findPC--;

	selectedCodeLine = wxNOT_FOUND;
	for (size_t i = 0; i < program.lineAddress.size(); i++)
	{
		if (program.lineAddress[i].address == findPC)
		{
			// Take the current line minus one because of 1-based line indexing
			selectedCodeLine = (int)program.lineAddress[i].line - 1;
			break;
		}
	}
	ListBoxCode->SetSelection(selectedCodeLine);
}

void DebuggerFrame::SetRunning(bool run)
{
	running = run;

	MenuItemProgramReset->Enable(!run);
	MenuItemProgramRun->Enable(!run);
	MenuItemProgramStop->Enable(run);
	MenuItemProgramStepCycle->Enable(!run);
	MenuItemProgramStepInstruction->Enable(!run);
}

void DebuggerFrame::OnReset(wxCommandEvent& event)
{
	if (wxMessageBox(_("Are you sure that you want to reset the program?"),
	                 _("Confirm Reset"),
	                 wxYES_NO | wxICON_QUESTION, this) == wxYES)
	{
		machine.loadProgram(program);

		UpdateUI();
	}
}

void DebuggerFrame::OnIdle(wxIdleEvent &event)
{
	if (running)
	{
		// Run some cycles
		for (size_t i = 0; i < 0x1000; i++)
			machine.stepCycle();

		// Auto stop if the machine halts
		if (machine.state == OpenSiMR::MachineState_Halt)
			SetRunning(false);

		// Update the GUI
		UpdateUI();

		event.RequestMore();
	}
}

void DebuggerFrame::OnRun(wxCommandEvent& event)
{
	/* To do this 'properly' we would have to create a separate thread,
	 * along with all the synchronization and resource management associated.
	 * Using wxYield is hacky but does the trick and is way easier. */
	SetRunning(true);
}

void DebuggerFrame::OnStop(wxCommandEvent& event)
{
	SetRunning(false);
}

void DebuggerFrame::OnStepCycle(wxCommandEvent& event)
{
	machine.stepCycle();

	UpdateUI();
}

void DebuggerFrame::OnStepInstruction(wxCommandEvent& event)
{
	machine.stepInstruction();

	UpdateUI();
}

void DebuggerFrame::OnListBoxCodeSelect(wxCommandEvent& event)
{
	// Prevent selection change by user
	ListBoxCode->SetSelection(selectedCodeLine);
}

void DebuggerFrame::OnCheckBoxRZClick(wxCommandEvent& event)
{
	// Prevent selection change by user
	CheckBoxRZ->SetValue(!CheckBoxRZ->GetValue());
}

void DebuggerFrame::OnCheckBoxRNClick(wxCommandEvent& event)
{
	// Prevent selection change by user
	CheckBoxRN->SetValue(!CheckBoxRN->GetValue());
}
