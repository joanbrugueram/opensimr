/*
	Copyright (C) 2013 Joan Bruguera Micó

	This file is part of OpenSiMR.

	OpenSiMR is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OpenSiMR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OpenSiMR.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EDITORFRAME_H
#define EDITORFRAME_H

#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/toolbar.h>
#include <wx/stc/stc.h>
#include <wx/fdrepdlg.h>

class EditorFrame: public wxFrame
{
	public:

		EditorFrame(wxWindow* parent, wxWindowID id=wxID_ANY);
		virtual ~EditorFrame();

	private:
		wxMenuBar *MainMenu;
		wxMenu *MenuFile;
		wxMenuItem *MenuFileNew;
		wxMenuItem *MenuFileOpen;
		wxMenuItem *MenuFileSave;
		wxMenuItem *MenuFileSaveAs;
		wxMenuItem *MenuFileQuit;
		wxMenu *MenuEdit;
		wxMenuItem *MenuEditUndo;
		wxMenuItem *MenuEditRedo;
		wxMenuItem *MenuEditCut;
		wxMenuItem *MenuEditCopy;
		wxMenuItem *MenuEditPaste;
		wxMenuItem *MenuEditSelectAll;
		wxMenuItem *MenuEditFind;
		wxMenuItem *MenuEditFindAndReplace;
		wxMenu *MenuProgram;
		wxMenuItem *MenuProgramRun;
		wxMenu *MenuBinary;
		wxMenuItem *MenuBinaryMakeCod;
		wxMenuItem *MenuBinaryRunCod;
		wxMenu *MenuHelp;
		wxMenuItem *MenuHelpAbout;
		wxMenuItem *MenuHelpLicense;

		wxToolBar *MainToolbar;
		wxToolBarToolBase *ToolBarNew;
		wxToolBarToolBase *ToolBarOpen;
		wxToolBarToolBase *ToolBarSave;
		wxToolBarToolBase *ToolBarSaveAs;
		wxToolBarToolBase *ToolBarUndo;
		wxToolBarToolBase *ToolBarRedo;
		wxToolBarToolBase *ToolBarCut;
		wxToolBarToolBase *ToolBarCopy;
		wxToolBarToolBase *ToolBarPaste;
		wxToolBarToolBase *ToolBarFind;
		wxToolBarToolBase *ToolBarFindAndReplace;
		wxToolBarToolBase *ToolBarRun;

		wxStyledTextCtrl *EditorCtrl;

		wxFindReplaceData FindData;
		wxFindReplaceDialog *FindDialog;

		wxString filePath;
		bool isChanged;

		void UpdateTitle();

		void NewFile();
		void LoadFromFilePath();
		void SaveToFilePath();

		bool AskFilePath(int kind);
		bool CanClose();

		void OnClose(wxCloseEvent &event);

		int TranslateSearchFlags(int flags);
		int SearchToTarget(const wxString &str, int start, int end);
		void SelectTarget();
		void DoFind(wxFindDialogEvent &event);

		void OnDoFind(wxFindDialogEvent &event);
		void OnDoFindNext(wxFindDialogEvent &event);
		void OnDoReplace(wxFindDialogEvent &event);
		void OnDoReplaceAll(wxFindDialogEvent &event);
		void OnDoFindClose(wxFindDialogEvent &event);

		void OnNew(wxCommandEvent &event);
		void OnOpen(wxCommandEvent &event);
		void OnSave(wxCommandEvent &event);
		void OnSaveAs(wxCommandEvent &event);
		void OnQuit(wxCommandEvent &event);
		void OnCut(wxCommandEvent &event);
		void OnCopy(wxCommandEvent &event);
		void OnPaste(wxCommandEvent &event);
		void OnSelectAll(wxCommandEvent &event);
		void OnFind(wxCommandEvent &event);
		void OnFindAndReplace(wxCommandEvent &event);
		void OnRun(wxCommandEvent &event);
		void OnMakeCod(wxCommandEvent &event);
		void OnRunCod(wxCommandEvent &event);
		void OnAbout(wxCommandEvent &event);
		void OnLicense(wxCommandEvent &event);
		void OnUndo(wxCommandEvent &event);
		void OnRedo(wxCommandEvent &event);
		void OnChange(wxStyledTextEvent &event);
		void OnSavePointReached(wxStyledTextEvent &event);
		void OnSavePointLeft(wxStyledTextEvent &event);
};

#endif
